from django.db import models

from apps.common.common_models import HospitalBaseModel


# Create your models here.
class PurchaseOrder(HospitalBaseModel):
    confirmed = models.BooleanField(default=False)
    order_total = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    # Relationships
    warehouse = models.ForeignKey('warehouse.Warehouse', on_delete=models.CASCADE)
    provider = models.ForeignKey('purchase.Provider', on_delete=models.CASCADE)

    class Meta:
        pass

    def __str__(self):
        return f'Orden de compra: {self.id}'


class PurchaseItem(HospitalBaseModel):
    unit_price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField()
    total_price = models.DecimalField(max_digits=10, decimal_places=2)
    added_to_stock = models.BooleanField(default=False)

    # Relationships
    purchase_order = models.ForeignKey('purchase.PurchaseOrder', on_delete=models.CASCADE)
    product = models.ForeignKey('warehouse.Product', on_delete=models.CASCADE)

    class Meta:
        pass

    def __str__(self):
        return f'Producto: {self.product}, Cantidad: {self.quantity}'


class SaleOrder(HospitalBaseModel):
    confirmed = models.BooleanField(default=False)
    description = models.CharField(max_length=3000, blank=True, null=True)

    order_total = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    # Relationships
    hospitalization = models.ForeignKey('patients.Hospitalization', on_delete=models.CASCADE, blank=True, null=True)
    warehouse = models.ForeignKey('warehouse.Warehouse', on_delete=models.CASCADE)
    provider = models.ForeignKey('purchase.Provider', on_delete=models.CASCADE)

    class Meta:
        pass

    def __str__(self):
        return f'Orden de compra: {self.id}'


class SaleItem(HospitalBaseModel):
    unit_price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    quantity = models.PositiveIntegerField()
    total_price = models.DecimalField(max_digits=10, decimal_places=2)
    added_to_stock = models.BooleanField(default=False)

    # Relationships
    sale_order = models.ForeignKey('purchase.SaleOrder', on_delete=models.CASCADE)
    product = models.ForeignKey('warehouse.Product', on_delete=models.CASCADE)

    class Meta:
        pass

    def __str__(self):
        return f'Producto: {self.product}, Cantidad: {self.quantity}'


class Provider(HospitalBaseModel):
    name = models.CharField(max_length=100)

    class Meta:
        pass

    def __str__(self):
        return f'{self.name}'
