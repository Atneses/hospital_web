# -*- coding: utf-8 -*-
from django.db.models.query_utils import Q
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.purchase.models import PurchaseOrder, SaleOrder
from apps.warehouse.models import Stock


@receiver(post_save, sender=PurchaseOrder)
def update_stock_purchase_order(sender, *args, **kwargs):
    """
    Update (increase) the stock for the items in the PurchaseOrder, as long as the PurchaseOrder is confirmed and the
    item was not previously updated yet by the same PurchaseOrder.

    :param sender:
    :param args:
    :param kwargs:
    :return: None
    """
    instance = kwargs['instance']
    if instance.confirmed and not instance.deleted:
        items = instance.purchaseitem_set.all()
        warehouse = instance.warehouse

        for i in items:
            quantity = i.quantity
            product = i.product
            f = Q(product=product) & Q(warehouse=warehouse)
            product_stock = Stock.objects.filter(f).first()
            if not i.added_to_stock:
                if product_stock and not i.added_to_stock:
                    product_stock.quantity += quantity
                    product_stock.save()
                else:
                    data = {
                        'quantity': quantity,
                        'warehouse': warehouse,
                        'product': product,
                    }
                    Stock.objects.create(**data)
                i.added_to_stock = True
                i.save()


@receiver(post_save, sender=SaleOrder)
def update_stock_sale_order(sender, *args, **kwargs):
    """
    Update (decreases) the stock for the items in the SaleOrder, as long as the SaleOrder is confirmed and the
    item was not previously updated yet by the same SaleOrder.

    :param sender:
    :param args:
    :param kwargs:
    :return:
    """
    instance = kwargs['instance']
    if instance.confirmed and not instance.deleted:
        items = instance.saleitem_set.all()
        warehouse = instance.warehouse

        for i in items:
            quantity = i.quantity
            product = i.product
            f = Q(product=product) & Q(warehouse=warehouse)
            product_stock = Stock.objects.filter(f).first()
            if not i.added_to_stock:
                if product_stock and not i.added_to_stock:
                    product_stock.quantity -= quantity
                    product_stock.save()
                else:
                    data = {
                        'quantity': quantity,
                        'warehouse': warehouse,
                        'product': product,
                    }
                    Stock.objects.create(**data)
                i.added_to_stock = True
                i.save()
