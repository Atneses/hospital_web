from django.contrib import admin
from safedelete.admin import SafeDeleteAdmin

from apps.common.common_models import BASE_FIELDS
from apps.purchase.models import PurchaseOrder, PurchaseItem, Provider, SaleOrder, SaleItem


# Register your models here.
class PurchaseItemInline(admin.TabularInline):
    model = PurchaseItem
    extra = 0
    readonly_fields = ['created_by', 'modified_by']
    fields = [
        'product',
        'unit_price',
        'quantity',
        'total_price',
        'added_to_stock',
        'purchase_order',
        'created_by',
        'modified_by'
    ]


@admin.register(PurchaseOrder)
class PurchaseOrderAdmin(SafeDeleteAdmin):
    list_display = ['id', 'confirmed', 'order_total', 'warehouse', 'provider', 'deleted', 'created']
    search_fields = ['order_total', 'provider__name', 'warehouse__name']
    readonly_fields = BASE_FIELDS
    ordering = ['-created']
    inlines = [PurchaseItemInline]


@admin.register(PurchaseItem)
class PurchaseItemAdmin(SafeDeleteAdmin):
    list_display = [
        'product',
        'quantity',
        'unit_price',
        'total_price',
        'purchase_order',
        'added_to_stock',
        'deleted'
    ]
    search_fields = [
        'unit_price',
        'unit_price',
        'quantity',
        'total_price',
        'added_to_stock',
        'product__name'
    ]
    readonly_fields = BASE_FIELDS


class SaleItemInline(admin.TabularInline):
    model = SaleItem
    extra = 0
    readonly_fields = BASE_FIELDS


@admin.register(SaleOrder)
class SaleOrderAdmin(SafeDeleteAdmin):
    list_display = ['confirmed', 'description', 'order_total', 'deleted']
    search_fields = ['confirmed', 'description', 'order_total']
    list_filter = ['confirmed']
    inlines = [SaleItemInline]
    readonly_fields = BASE_FIELDS


@admin.register(SaleItem)
class SaleItemAdmin(SafeDeleteAdmin):
    list_field = ['unit_price', 'quantity', 'total_price', 'added_to_stock']
    search_fields = ['unit_price', 'quantity', 'total_price', 'added_to_stock']
    list_filter = ['added_to_stock']
    readonly_fields = BASE_FIELDS


@admin.register(Provider)
class ProviderAdmin(SafeDeleteAdmin):
    list_display = ['name', 'deleted']
    search_fields = ['name']
    readonly_fields = BASE_FIELDS
