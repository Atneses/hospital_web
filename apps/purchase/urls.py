# -*- coding: utf-8 -*-
from django.urls import path, include
from rest_framework import routers

from apps.purchase import views

router = routers.DefaultRouter()
router.register('orders', views.PurchaseOrderViewSet)
router.register('sales', views.SaleOrderViewSet)
router.register('items', views.PurchaseItemViewSet)
router.register('sale_items', views.SaleItemViewSet)
router.register('providers', views.ProviderViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('items-search/<str:search>', views.PurchaseItemSearchView.as_view(), name='purchase-items-search'),
]
