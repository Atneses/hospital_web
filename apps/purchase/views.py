from typing import Any

from django.db.models import QuerySet, Q
from rest_framework import status
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response

from apps.common.common_views import HospitalBaseViewSet, HospitalBaseListAPIView
from apps.purchase.models import PurchaseOrder, PurchaseItem, Provider, SaleOrder, SaleItem
from apps.purchase.serializers import PurchaseOrderFullSerializer, PurchaseItemFullSerializer, ProviderFullSerializer, \
    SaleOrderFullSerializer, SaleItemFullSerializer, PurchaseItemSmallSerializer, PurchaseOrderSmallSerializer, \
    SaleOrderSmallSerializer, SaleItemSmallSerializer, ProviderSmallSerializer


# Create your views here.
class SaleOrderViewSet(HospitalBaseViewSet):
    permission_classes = [IsAuthenticated]
    queryset = SaleOrder.objects.all()
    serializer_class = SaleOrderFullSerializer
    small_serializer_class = SaleOrderSmallSerializer


class SaleItemViewSet(HospitalBaseViewSet):
    permission_classes = [IsAuthenticated]
    queryset = SaleItem.objects.all()
    serializer_class = SaleItemFullSerializer
    small_serializer_class = SaleItemSmallSerializer

    def create(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        serializer = SaleItemFullSerializer(data=request.data, many=isinstance(request.data, list))
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PurchaseOrderViewSet(HospitalBaseViewSet):
    permission_classes = [IsAuthenticated]
    queryset = PurchaseOrder.objects.all()
    serializer_class = PurchaseOrderFullSerializer
    small_serializer_class = PurchaseOrderSmallSerializer


class PurchaseItemViewSet(HospitalBaseViewSet):
    permission_classes = [IsAuthenticated]
    queryset = PurchaseItem.objects.all()
    serializer_class = PurchaseItemFullSerializer
    small_serializer_class = PurchaseItemSmallSerializer

    def create(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        serializer = PurchaseItemFullSerializer(data=request.data, many=isinstance(request.data, list))
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PurchaseItemSearchView(HospitalBaseListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = PurchaseItem.objects.all()
    serializer_class = PurchaseItemSmallSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self) -> QuerySet:
        search = self.kwargs['search']

        q = Q(product__brand__icontains=search) |\
            Q(product__barcode__icontains=search) |\
            Q(product__product_type__icontains=search) |\
            Q(product__presentation__icontains=search) |\
            Q(product__cost__icontains=search) |\
            Q(product__application_cost__icontains=search) |\
            Q(product__active_substance__icontains=search) |\
            Q(product__dosage__icontains=search) |\
            Q(created_by__username=search)
        if search.isdigit():
            n_search = int(search)
            q |= Q(unit_price=n_search) | Q(quantity=n_search) | Q(total_price=n_search)
        results = PurchaseItem.objects.filter(q).all()
        return results


class ProviderViewSet(HospitalBaseViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Provider.objects.all()
    serializer_class = ProviderFullSerializer
    small_serializer_class = ProviderSmallSerializer
