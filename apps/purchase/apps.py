from django.apps import AppConfig


class PurchasesConfig(AppConfig):
    name = 'apps.purchase'

    def ready(self):
        import apps.purchase.signals
