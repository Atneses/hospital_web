# -*- coding: utf-8 -*-
import logging
from typing import Any

from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import ModelSerializer

from apps.common.common_models import BASE_FIELDS
from apps.common.common_serializers import HospitalBaseSerializer, CustomUserShortSerializer
from apps.patients.models import Hospitalization
from apps.patients.serializers import HospitalizationFullSerializer
from apps.profiles.serializers import CustomUserSerializer
from apps.purchase.models import PurchaseOrder, PurchaseItem, Provider, SaleOrder, SaleItem
from apps.warehouse.models import Product, Warehouse
from apps.warehouse.serializers import GenericProductSerializer, WarehouseSerializerForStock, ProductShallowSerializer

logger = logging.getLogger(__name__)


class SaleOrderFullSerializer(HospitalBaseSerializer):
    provider = PrimaryKeyRelatedField(queryset=Provider.objects.all())
    hospitalization = PrimaryKeyRelatedField(queryset=Hospitalization.objects.all())
    warehouse = PrimaryKeyRelatedField(queryset=Warehouse.objects.all())
    saleitem_set = PrimaryKeyRelatedField(read_only=True, many=True)

    class Meta(HospitalBaseSerializer.Meta):
        model = SaleOrder

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['hospitalization'] = HospitalizationFullSerializer(instance.hospitalization).data
        data['warehouse'] = WarehouseSerializerForStock(instance.warehouse).data
        data['provider'] = ProviderFullSerializer(instance.provider).data
        data['saleitem_set'] = SaleItemFullSerializer(instance.saleitem_set, many=True).data
        return data


class SaleOrderSmallSerializer(ModelSerializer):
    class Meta:
        model = SaleOrder
        exclude = ['confirmed', 'description', 'order_total']


class SaleItemFullSerializer(HospitalBaseSerializer):
    product = PrimaryKeyRelatedField(queryset=Product.objects.all())
    sale_order = PrimaryKeyRelatedField(queryset=SaleOrder.objects.all())

    class Meta(HospitalBaseSerializer.Meta):
        fields = None
        model = SaleItem
        exclude = ['added_to_stock']

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['product'] = GenericProductSerializer(instance.product).data
        logger.info(data['product'])
        data['unit_price'] = data['product']['cost']
        return data


class SaleItemSmallSerializer(ModelSerializer):
    class Meta:
        model = SaleItem
        exclude = ['sale_order', 'product']


class PurchaseOrderFullSerializer(HospitalBaseSerializer):
    provider = PrimaryKeyRelatedField(queryset=Provider.objects.all())
    warehouse = PrimaryKeyRelatedField(queryset=Warehouse.objects.all())
    purchaseitem_set = PrimaryKeyRelatedField(read_only=True, many=True)

    class Meta(HospitalBaseSerializer.Meta):
        model = PurchaseOrder

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['warehouse'] = WarehouseSerializerForStock(instance.warehouse).data
        data['provider'] = ProviderFullSerializer(instance.provider).data
        data['purchaseitem_set'] = PurchaseItemFullSerializer(instance.purchaseitem_set, many=True).data
        return data


class PurchaseOrderSmallSerializer(ModelSerializer):
    class Meta:
        model = PurchaseOrder
        exclude = ['warehouse', 'provider']


class PurchaseItemFullSerializer(HospitalBaseSerializer):
    product = PrimaryKeyRelatedField(queryset=Product.objects.all())
    purchase_order = PrimaryKeyRelatedField(queryset=PurchaseOrder.objects.all())

    class Meta(HospitalBaseSerializer.Meta):
        fields = None
        model = PurchaseItem
        exclude = ['added_to_stock']

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['product'] = GenericProductSerializer(instance.product).data
        return data


class PurchaseItemSmallSerializer(ModelSerializer):
    class Meta:
        model = PurchaseItem
        exclude = ['purchase_order', 'product']

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['created_by'] = CustomUserShortSerializer(instance.created_by).data
        data['product'] = ProductShallowSerializer(instance.product).data

        return data


class ProviderFullSerializer(HospitalBaseSerializer):

    class Meta(HospitalBaseSerializer.Meta):
        model = Provider


class ProviderSmallSerializer(ModelSerializer):

    class Meta:
        model = Provider
        exclude = BASE_FIELDS[1:]
