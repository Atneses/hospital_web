# Generated by Django 2.2 on 2021-05-13 23:24

from django.db import migrations

from apps.common.groups_variables import MEDIC_GROUP_ID, CHECKOUT_GROUP_ID, RECEPTION_GROUP_ID, WAREHOUSE_GROUP_ID


def forward(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    Permission = apps.get_model('auth', 'Permission')
    Charge = apps.get_model('patients', 'Charge')
    ContentType = apps.get_model('contenttypes', 'ContentType')
    content_type = ContentType.objects.get_for_model(Charge)

    delete_charge = Permission.objects.filter(content_type=content_type, codename='delete_charge').first()
    groups = Group.objects.filter(id__in=[MEDIC_GROUP_ID, RECEPTION_GROUP_ID, CHECKOUT_GROUP_ID, WAREHOUSE_GROUP_ID])
    print(groups)
    for group in groups:
        group.permissions.add(delete_charge)


def backwards(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    Permission = apps.get_model('auth', 'Permission')
    Charge = apps.get_model('patients', 'Charge')
    ContentType = apps.get_model('contenttypes', 'ContentType')
    content_type = ContentType.objects.get_for_model(Charge)

    delete_charge = Permission.objects.filter(content_type=content_type, codename='delete_charge').first()
    groups = Group.objects.filter(id__in=[MEDIC_GROUP_ID, RECEPTION_GROUP_ID, CHECKOUT_GROUP_ID, WAREHOUSE_GROUP_ID])
    for group in groups:
        group.permissions.remove(delete_charge)


class Migration(migrations.Migration):
    dependencies = [
        ('profiles', '0005_add_change_charge_permissions_nurse_and_warehouse'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=backwards)
    ]
