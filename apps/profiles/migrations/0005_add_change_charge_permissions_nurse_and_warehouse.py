# Generated by Django 2.2 on 2021-03-18 05:12
from django.db import migrations

from apps.common.groups_variables import WAREHOUSE_GROUP_ID


def forward(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    Permission = apps.get_model('auth', 'Permission')
    Charge = apps.get_model('patients', 'Charge')
    ContentType = apps.get_model('contenttypes', 'ContentType')
    content_type = ContentType.objects.get_for_model(Charge)
    change_charge = Permission.objects.filter(content_type=content_type, codename='change_charge').first()
    delete_charge = Permission.objects.filter(content_type=content_type, codename='delete_charge').first()
    groups = Group.objects.filter(id__in=[WAREHOUSE_GROUP_ID])
    for group in groups:
        group.permissions.add(change_charge)
        group.permissions.add(delete_charge)


def backwards(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    Permission = apps.get_model('auth', 'Permission')
    Charge = apps.get_model('patients', 'Charge')
    ContentType = apps.get_model('contenttypes', 'ContentType')
    content_type = ContentType.objects.get_for_model(Charge)
    change_charge = Permission.objects.filter(content_type=content_type, codename='change_charge').first()
    delete_charge = Permission.objects.filter(content_type=content_type, codename='delete_charge').first()
    groups = Group.objects.filter(id__in=[WAREHOUSE_GROUP_ID])
    for group in groups:
        group.permissions.remove(change_charge)
        group.permissions.remove(delete_charge)


class Migration(migrations.Migration):
    dependencies = [
        ('profiles', '0004_auto_20201023_1708'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=backwards)
    ]
