# -*- coding: utf-8 -*-
from typing import Any

from django.contrib.auth.models import Group
from django.db.models.base import Model
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import ModelSerializer

from apps.common.common_serializers import HospitalBaseSerializer, CustomUserShortSerializer
from apps.patients.models import Charge, Document
from apps.profiles.models import CustomUser, ProfilePhysician, PhysicianAction
from apps.warehouse.models import Warehouse


class GroupSerializer(ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'name']


class WarehouseUserSerializer(ModelSerializer):
    """
    Warehouse for the user endpoints
    """

    class Meta:
        model = Warehouse
        fields = '__all__'


class CustomUserSerializer(ModelSerializer):
    groups = PrimaryKeyRelatedField(many=True, queryset=Group.objects.all())
    warehouse = PrimaryKeyRelatedField(queryset=Warehouse.objects.all(), allow_null=True, required=False)

    class Meta:
        model = CustomUser
        fields = '__all__'
        depth = 1

    def create(self, validated_data: Any) -> Any:
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, instance: Model, validated_data: Any) -> Any:
        user = super().update(instance, validated_data)

        if 'password' in validated_data:
            user.set_password(validated_data['password'])
            user.save()
        return user

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        del data['password']
        data['groups'] = GroupSerializer(instance.groups, many=True).data
        if instance.warehouse:
            data['warehouse'] = WarehouseUserSerializer(instance.warehouse).data
        if hasattr(instance, 'profile_physician'):
            data['actions'] = PhysicianActionsShortSerializer(instance.profile_physician.actions, many=True).data
            data['profile_physician'] = ProfilePhysicianShortSerializer(instance.profile_physician).data
        return data


class ProfilePhysicianShortSerializer(ModelSerializer):
    class Meta:
        model = ProfilePhysician
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['user'] = CustomUserShortSerializer(instance.user).data
        return data


class ProfilePhysicianSerializer(ModelSerializer):
    user = PrimaryKeyRelatedField(queryset=CustomUser.objects.all())

    class Meta:
        model = ProfilePhysician
        fields = '__all__'
        depth = 1

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['actions'] = PhysicianActionsShortSerializer(PhysicianAction.objects.filter(physician_id=instance.id),
                                                          many=True).data
        data['user'] = CustomUserShortSerializer(instance.user).data
        return data


class ChargeSmallSerializer(ModelSerializer):
    class Meta:
        model = Charge
        exclude = ['hospitalization', 'charge_type', 'product', 'warehouse', 'physician']

    def to_representation(self, instance: Charge):
        data = super().to_representation(instance)
        data['patient'] = instance.hospitalization.patient.full_name
        data['hospitalization_id'] = instance.hospitalization.id
        data['can_pay_honorariums'] = instance.hospitalization.can_pay_honorariums
        return data


class PhysicianUnpaidSerializer(ModelSerializer):

    class Meta:
        model = ProfilePhysician
        fields = '__all__'

    def to_representation(self, instance: ProfilePhysician):
        data = super().to_representation(instance)
        data['user'] = CustomUserShortSerializer(instance.user).data
        honorariums = instance.user.charge_set.filter(paid__isnull=True)
        paid_honorariums = instance.user.charge_set.all().filter(paid__isnull=False)
        data['honorariums'] = ChargeSmallSerializer(honorariums, many=True).data
        data['paid_honorariums'] = ChargeSmallSerializer(paid_honorariums, many=True).data
        return data


class PhysicianActionsShortSerializer(ModelSerializer):
    created_by = PrimaryKeyRelatedField(read_only=True)
    modified_by = PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = PhysicianAction
        fields = '__all__'


class DocumentSerializer(HospitalBaseSerializer):
    class Meta:
        model = Document
        fields = '__all__'
