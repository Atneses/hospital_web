# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import Group, Permission
from safedelete.admin import SafeDeleteAdmin

from apps.common.common_models import BASE_FIELDS
from apps.profiles.models import CustomUser, ProfilePhysician, PhysicianAction


# Register your models here.
# region Built-in models
class GroupsAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    filter_horizontal = ['permissions']

    class Meta:
        model = Group
        ordering = ['id']


admin.site.unregister(Group)
admin.site.register(Group, GroupsAdmin)


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    list_display = ['name', 'content_type', 'codename']
    search_fields = ['name', 'codename']

# endregion


class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = CustomUser


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    form = CustomUserChangeForm
    list_display = ('id',) + UserAdmin.list_display + ('is_warehouse_user',)
    fieldsets = UserAdmin.fieldsets + (
        ('Custom fields', {
            'fields': ('curp', 'avatar', 'first_login', 'warehouse', 'is_warehouse_user', 'is_physician')}),
    )
    readonly_fields = ['is_physician', 'is_warehouse_user']


@admin.register(ProfilePhysician)
class ProfilePhysicianAdmin(SafeDeleteAdmin):
    search_fields = [
        'mobile_phone',
        'university_name',
        'college_initials',
        'professional_id',
        'hospital_specialty',
        'university_of_specialty',
        'specialty_id',
        'street',
        'outdoor_number',
        'interior_number',
        'municipality',
        'state',
        'zip_code'
    ]
    list_display = [
        'university_name',
        'professional_id',
        'hospital_specialty',
        'university_of_specialty',
        'specialty_id',
        'user',
        'paid_honorariums',
        'unpaid_honorariums',
        'deleted'
    ]
    readonly_fields = BASE_FIELDS


@admin.register(PhysicianAction)
class PhysicianActionAdmin(SafeDeleteAdmin):
    search_fields = ['_type', 'number']
    list_display = ['_type', 'number', 'physician', 'deleted']
    readonly_fields = BASE_FIELDS
