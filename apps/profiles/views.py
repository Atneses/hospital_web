# -*- coding: utf-8 -*-
import logging
from typing import Type, Any

from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import Group
from django.db import transaction
from django.db.models import Q, Count
from django.utils import timezone
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import BaseSerializer
from rest_framework.serializers import ValidationError
from rest_framework.views import APIView

from apps.profiles.models import CustomUser, ProfilePhysician, PhysicianAction
from apps.profiles.serializers import CustomUserSerializer, GroupSerializer, ProfilePhysicianShortSerializer, \
    PhysicianUnpaidSerializer, ProfilePhysicianSerializer, PhysicianActionsShortSerializer

logger = logging.getLogger("apps.profiles")


# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer

    def get_queryset(self):
        # Filter query set to only return users that belong to a group.
        queryset = super().get_queryset()
        queryset = queryset.exclude(groups=None)
        return queryset

    @staticmethod
    def create_physician_user(data, user):
        """
        Create a ProfilePhysician and assign to a user if the user is in the group 1
        :param data: Data for create ProfilePhysician object
        :param user: The user that has a Physician Profile
        :return: ProfilePhysician instance
        """
        data['user'] = user
        physician = ProfilePhysician.objects.create(**data)
        return physician

    def create(self, request: Request, *args, **kwargs) -> Response:
        if 1 in request.data['groups']:
            with transaction.atomic():
                response = super().create(request)
                user = response.data.serializer.instance
                self.create_physician_user(request.data.pop('profile_physician'), user)
                return Response(CustomUserSerializer(user).data, status=status.HTTP_201_CREATED)
        else: 
            return super().create(request)
    
    @action(detail=False, methods=['post'])
    def change_password(self, request, *args, **kwargs):
        if request.data['new_password1'] is not None and request.data['new_password1'] == request.data['new_password1']:
            request.user.set_password(request.data['new_password1'])
            request.user.last_login = timezone.now()
            request.user.save()
            update_session_auth_hash(request, request.user)  # Important!
            return Response({"message": 'Your password was successfully updated!'})
        else:
            raise ValidationError({"message": "passwords does not match"})
            

# region Profiles Views
class ProfilePhysicianViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = ProfilePhysician.objects.all()
    serializer_class = ProfilePhysicianShortSerializer
    pagination_class = LimitOffsetPagination

    def get_serializer_class(self) -> Type[BaseSerializer]:
        if self.action != 'list':
            return ProfilePhysicianSerializer
        return super().get_serializer_class()

    @action(detail=False)
    def unpaid(self, request, *args, **kwargs):
        not_paid = Q(user__charge__paid__isnull=True)
        paid = Q(user__charge__paid__isnull=False)

        queryset = self.queryset.annotate(
            honorariums=Count('user__charge', filter=not_paid),
            honorariums_paid=Count('user__charge', filter=paid)
        )
        queryset = queryset.filter(honorariums__gt=0).order_by('user__first_name', 'user__last_name', 'honorariums')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = PhysicianUnpaidSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = PhysicianUnpaidSerializer(self.queryset, many=True)
        return Response(serializer.data)


class PhysicianActionsViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = PhysicianAction.objects.all()
    serializer_class = PhysicianActionsShortSerializer
    pagination_class = LimitOffsetPagination

    def create(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        serializer = PhysicianActionsShortSerializer(data=request.data, many=isinstance(request.data, list))
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)
    
    @action(detail=False, methods=['put'])
    def update_bulk(self, request, *args, **kwargs):
        updated = []
        deleted = []
        with transaction.atomic():
            for _action in request.data['update']:
                current_action = PhysicianAction.objects.get(id=_action['id'])
                logger.info("\n\n")
                logger.info(current_action.id)
                logger.info("\n\n")
                current_action._type = _action['_type']
                current_action.number = _action['number']
                current_action.save()
                updated.append(self.serializer_class().to_representation(current_action))
            for action_id in request.data['delete']:
                current_action = PhysicianAction.objects.get(id=action_id)
                current_action.delete()
                deleted.append(action_id)

        return Response({"updated": updated, "deleted": deleted})

# endregion


class GroupViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class Me(APIView):
    """
    Return the current user information
    """
    permission_classes = [IsAuthenticated]

    def get(self, request):
        data = CustomUserSerializer(request.user).data
        return Response(data)
