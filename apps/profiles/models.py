# -*- coding: utf-8 -*-
from django.contrib.auth.models import User, AbstractUser
from django.db import models
from django.db.models import Q

from apps.common.common_models import HospitalBaseModel
from apps.common.groups_variables import MEDIC_GROUP_ID, WAREHOUSE_GROUP_ID, CHECKOUT_GROUP_ID, RECEPTION_GROUP_ID, \
    NURSING_GROUP_ID


# Create your models here.
class CustomUser(AbstractUser):
    curp = models.CharField(
        max_length=18,
        verbose_name='CURP',
        blank=True,
        null=True,
        help_text='Mexican unique ID'
    )
    avatar = models.CharField(max_length=200, blank=True, null=True)
    first_login = models.BooleanField(
        default=True,
        help_text='Default True, the first time the user is logged this in change to False'
    )

    # Relation ships
    warehouse = models.ForeignKey('warehouse.Warehouse', on_delete=models.CASCADE, blank=True, null=True)

    @property
    def is_warehouse_user(self) -> bool:
        groups = self.groups.filter(id=WAREHOUSE_GROUP_ID).all()
        return True if groups else False

    @property
    def is_physician(self) -> bool:
        """
        Return True if the user is in the 'Médico' group else return false
        :return: bool
        """
        groups = self.groups.filter(Q(id=MEDIC_GROUP_ID)).all()
        return True if groups else False

    @property
    def is_checkout_user(self):
        groups = self.groups.filter(Q(id=CHECKOUT_GROUP_ID)).all()
        return True if groups else False

    @property
    def is_reception_user(self):
        groups = self.groups.filter(Q(id=RECEPTION_GROUP_ID)).all()
        return True if groups else False

    @property
    def is_nurse(self):
        """
        Return True if the user is in the 'Enfermería' group else return false
        :return: bool
        """
        groups = self.groups.filter(Q(id=NURSING_GROUP_ID)).all()
        return True if groups else False


class ProfilePhysician(HospitalBaseModel):
    mobile_phone = models.CharField(max_length=50)
    # region Scholarship
    university_name = models.CharField(max_length=200)
    college_initials = models.CharField(max_length=20)
    professional_id = models.CharField(max_length=20)
    # endregion

    # region Specialty
    hospital_specialty = models.CharField(max_length=200, blank=True, null=True)
    university_of_specialty = models.CharField(max_length=200, blank=True, null=True)
    specialty_id = models.CharField(max_length=20, blank=True, null=True)
    # endregion

    # region Physician's Office
    street = models.CharField(max_length=200)
    outdoor_number = models.CharField(max_length=200)
    interior_number = models.CharField(max_length=200, null=True)
    municipality = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    zip_code = models.CharField(max_length=10)
    # endregion

    # Relationships
    user = models.OneToOneField('CustomUser', related_name='profile_physician', on_delete=models.CASCADE)

    class Meta:
        pass

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'

    @property
    def paid_honorariums(self):
        return self.user.charge_set.filter(paid__isnull=False).count()

    @property
    def unpaid_honorariums(self):
        return self.user.charge_set.filter(paid__isnull=True).count()


class PhysicianAction(HospitalBaseModel):
    _type = models.CharField(max_length=100, blank=True, null=True)
    number = models.CharField(max_length=100, blank=True, null=True)

    # Relationships
    physician = models.ForeignKey('ProfilePhysician', related_name='actions', on_delete=models.CASCADE)

    class Meta:
        pass

    def __str__(self):
        return f'{self._type}'
