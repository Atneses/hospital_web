# -*- coding: utf-8 -*-
from django.urls import path, include
from rest_framework import routers

from apps.profiles import views

router = routers.DefaultRouter()
router.register('users', views.UserViewSet)
router.register('groups', views.GroupViewSet)
router.register('physician', views.ProfilePhysicianViewSet),
router.register('physician-actions', views.PhysicianActionsViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('me/', views.Me.as_view(), name='me'),
]
