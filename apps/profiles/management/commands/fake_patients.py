# -*- coding: utf-8 -*-
import random
from typing import Any

from crum import impersonate
from django.core.management.base import BaseCommand
from faker import Faker

from apps.patients.models import Patient, Relative, Hospitalization, ENTRY, PRE_HIGH, HIGH
from apps.profiles.models import CustomUser
from apps.spaces.models import Space

CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
GENDERS = ['MALE', 'FEMALE']
RELATIONS = [
    'Madre',
    'Padre',
    'Hermano/a',
    'Primo',
    'Tio/a',
    'Sobrino/a',
    'Amigo/a',
    'Vecino/a',
    'Conocido/a',
    'Otro/a'
]


class Command(BaseCommand):
    fake = Faker()

    def gen_common(self):
        data = {
            'name': self.fake.name(),
            'father_last_name': self.fake.last_name(),
            'mother_last_name': self.fake.last_name(),
            'country': self.fake.country(),
            'state': self.fake.state(),
            'municipality': self.fake.city(),
            'location': self.fake.city(),
            'colony': self.fake.city(),
            'street': self.fake.street_name(),
            'ext_number': str(random.randint(10, 10000)),
            'int_number': str(random.randint(10, 10000)),
            'between_streets': f'Entre {self.fake.street_name()} y {self.fake.street_name()}'
        }
        return data

    def gen_patient(self):
        common = self.gen_common()
        patient = {
            'phone': self.fake.phone_number(),
            'curp': ''.join(random.choices(CHARS, k=18)),
            'cel_phone': self.fake.phone_number(),
            'gender': random.choice(GENDERS),
            'dob': self.fake.date_this_century(before_today=True, after_today=False),
            'occupation': self.fake.job()
        }
        patient.update(common)
        return patient

    def gen_relative(self):
        common = self.gen_common()
        data = {}
        data.update(common)
        return data

    def gen_hospitalizations(self):
        patients = list(Patient.objects.all())
        random.shuffle(patients)
        relatives = Relative.objects.all()
        states = [ENTRY, PRE_HIGH, HIGH]

        for space in Space.objects.all():
            data = {
                'relation': random.choice(RELATIONS),
                'patient': patients.pop(),
                'relative': random.choice(relatives),
                'space': space,
                'space_cost': space.cost,
                'state': random.choice(states)
            }
            Hospitalization.objects.create(**data)
            print(data)

    def handle(self, *args: Any, **options: Any):
        admin = CustomUser.objects.get(username='admin')
        with impersonate(admin):
            for x in range(0, 50):
                patient = self.gen_patient()
                relative = self.gen_relative()
                p = Patient.objects.create(**patient)
                print(p)
                r = Relative.objects.create(**relative)
                print(r)

            self.gen_hospitalizations()
