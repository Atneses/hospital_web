# -*- coding: utf-8 -*-
import json
import pathlib

from crum import impersonate
from django.contrib.auth.models import Group, Permission
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from apps.profiles.models import CustomUser
from apps.warehouse.models import Presentation, Product, PRODUCT, MEDICINE


class Command(BaseCommand):
    MEDICINES = 'medicines.json'
    PRESENTATION = 'presentations.json'
    PRODUCT = 'products.json'
    USERS = 'users.json'

    def open_file(self, file_name):
        path = str(pathlib.Path(__file__).parent.absolute()) + '/data/'

        with open(path + file_name) as _file:
            data = json.loads(_file.read())
            return data

    def handle(self, *args, **kwargs):
        admin = CustomUser.objects.get(username='admin')
        with impersonate(admin):
            # region CREATE USERS
            users_data = self.open_file(self.USERS)
            created_users = []
            for user in users_data:
                fields = user['fields']
                groups = fields['groups']
                permissions = fields['user_permissions']
                del fields['groups']
                del fields['user_permissions']
                try:
                    us = CustomUser.objects.create(**fields)

                    for g in groups:
                        group = Group.objects.get(pk=g)
                        group.user_set.add(us)

                    for p in permissions:
                        permission = Permission.objects.get(pk=p)
                        permission.user_set.add(us)
                    created_users.append(us)
                except Exception as error:
                    print(f'USER CREATION: {error}')
            # endregion

            # region CREATE PRESENTATIONS
            presentation_data = self.open_file(self.PRESENTATION)
            created_presentations = []
            for pre in presentation_data:
                fields = pre['fields']
                fields['pk'] = pre['pk']
                try:
                    created_presentations.append(
                        Presentation.objects.create(**fields)
                    )
                    print(fields)
                except IntegrityError as error:
                    print(f'Already exists: {fields["pk"]}')
            print(f'{len(created_presentations)} presentations created')
            # endregion

            # region CREATE PRODUCTS
            products_data = self.open_file(self.PRODUCT)
            created_products = []
            for pro in products_data:
                fields = pro['fields']
                # fields['pk'] = pro['pk']
                fields['product_type'] = PRODUCT
                fields['cost'] = 0
                fields['application_cost'] = 0
                del fields['quantity']
                try:
                    created_products.append(
                        Product.objects.create(**fields)
                    )
                    print(fields)
                except IntegrityError as error:
                    print(f'Already exists {fields}')
            # endregion

            # region CREATE MEDICINES
            medicines_data = self.open_file(self.MEDICINES)
            created_medicines = []
            for med in medicines_data:
                fields = med['fields']
                # fields['pk'] = med['pk']
                fields['presentation_medicine'] = Presentation.objects.get(pk=fields['presentation'])
                fields['product_type'] = MEDICINE
                fields['name'] = fields['active_substance']
                fields['cost'] = 0
                fields['application_cost'] = 0
                del fields['presentation']
                del fields['quantity']
                try:
                    created_medicines.append(
                        Product.objects.create(**fields)
                    )
                    print(fields)
                except IntegrityError as error:
                    print(f'Already exists {fields}')
            # endregion
