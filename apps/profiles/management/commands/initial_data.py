# -*- coding: utf-8 -*-
import logging

from crum import impersonate
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand

from apps.patients.models import ChargeType
from apps.profiles.models import CustomUser

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Initial data'

    def create_groups(self):
        groups = [
            {'id': 1, 'name': 'Médico'},
            {'id': 2, 'name': 'Enfermería'},
            {'id': 3, 'name': 'Recepción'},
            {'id': 4, 'name': 'Almacén'},
            {'id': 5, 'name': 'Caja'},
            {'id': 6, 'name': 'Administración'},
            {'id': 7, 'name': 'Dirección'}
        ]
        for group in groups:
            try:
                g = Group.objects.create(name=group)
                print(f'Group {g} created')
            except Exception as error:
                message = f'{error}, NAME: "{group}"'
                logger.error(message)

    def create_admin(self):
        # Create superuser
        superuser_data = {
            'username': 'admin',
            'email': 'admin@admin.com',
            'password': 'admin1'
        }
        print(superuser_data)
        try:
            CustomUser.objects.create_superuser(**superuser_data)
        except Exception as error:
            message = f'{error}, USERNAME: "{superuser_data["username"]}"'
            logger.error(message)

    def create_charges_types(self, user):
        types = [
            {'id': 1, 'name': 'Honorario'},
            {'id': 2, 'name': 'Insumo'},
            {'id': 3, 'name': 'Medicamento'},
            {'id': 4, 'name': 'Otro'},
        ]
        with impersonate(user):
            for charge_type in types:
                try:
                    print(f'Creating new charge type "{charge_type["name"]}"')
                    ChargeType.objects.create(**charge_type)
                except Exception as error:
                    message = f'{error} with data {charge_type}'
                    logger.error(message)

    def handle(self, *args, **kwargs):
        self.create_groups()
        self.create_admin()

        admin = CustomUser.objects.get(pk=1)
        if admin:
            self.create_charges_types(admin)
        else:
            message = 'Not admin found, can\'t create the initial data'
            logger.error(message)
            raise Exception(message)
