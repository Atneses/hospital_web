# -*- coding: utf-8 -*-
import random

from crum import impersonate
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand
from faker import Faker

from apps.profiles.models import CustomUser
from apps.purchase.models import Provider
from apps.spaces.models import Area, Type, Space
from apps.warehouse.models import Warehouse, Product, PRODUCT, MEDICINE, Presentation, Stock


class Command(BaseCommand):
    help = 'Populate with fake data, only for use in develop environment.'
    faker = Faker()

    def create_groups(self):
        groups = [
            'Médico',
            'Enfermería',
            'Recepción',
            'Almacén',
            'Caja',
            'Administración',
            'Dirección'
        ]
        for group in groups:
            try:
                g = Group.objects.create(name=group)
                print(f'Group {g} created')
            except Exception as error:
                pass

    def create_users(self):
        # Create superuser
        username = 'admin'
        superuser_data = {
            'username': username,
            'email': 'admin@admin.com',
            'password': 'admin1'
        }
        try:
            superuser = CustomUser.objects.create_superuser(**superuser_data)
            print(superuser_data)
        except Exception as error:
            superuser = CustomUser.objects.get(username=username)

        groups = Group.objects.all()
        for user_number in range(0, 5):
            try:
                data = {
                    'username': f'user_{user_number}',
                    'email': f'user_{user_number}@email.com',
                    'password': f'user_{user_number}',
                    # 'groups': [random.choice(groups)]
                }
                user = CustomUser.objects.create_user(**data)

                g = random.choice(groups)
                g.user_set.add(user)
                print(data)
            except Exception as error:
                pass
        return superuser

    def create_areas(self):
        for a in range(0, 10):
            data = {
                'area_name': f'Area {a}'
            }
            try:
                Area.objects.create(**data)
                print(data)
            except Exception as error:
                print(error)

    def create_types(self):
        for t in range(0, 10):
            data = {
                'type_name': f'Type {t}'
            }
            print(data)
            Type.objects.create(**data)

    def create_spaces(self):
        areas = Area.objects.all()
        types = Type.objects.all()

        for s in range(0, 6):
            data = {
                'number': f'Number {s}',
                'area': random.choice(areas),
                'space_type': random.choice(types),
                'cost': self.faker.pydecimal(left_digits=3, right_digits=2, positive=True, min_value=10,
                                             max_value=10000),
            }
            print(data)
            Space.objects.create(**data)

    def create_warehouse(self):
        warehouses = []
        for number in range(0, 5):
            data = {'name': f'Almacen {number}'}
            warehouses.append(Warehouse.objects.create(**data))
        return warehouses

    def create_pesentation_medicine(self):
        for number in range(0, 10):
            Presentation.objects.create(presentation_type=f'Presentacion {number}')

    def create_products(self):
        products = []
        for number in range(0, 10):
            try:
                data = {
                    'name': f'Producto {number}',
                    'brand': f'Producto {number}',
                    'barcode': f'Producto_{random.randint(9999, 99999)}',
                    'product_type': PRODUCT,
                    'presentation': f'Producto {number}',
                    'cost': self.faker.pydecimal(left_digits=3, right_digits=2, positive=True, min_value=10,
                                                 max_value=10000),
                    'application_cost': self.faker.pydecimal(left_digits=3, right_digits=2, positive=True, min_value=10,
                                                             max_value=10000)
                }
                products.append(Product.objects.create(**data))
                print(data)
            except Exception as erro:
                pass

    def create_medicine(self):
        medicines = []
        presentations = Presentation.objects.all()
        for number in range(0, 10):
            data = {
                'name': f'Medicine {number}',
                'brand': f'Medicine {number}',
                'barcode': f'Medicine_{random.randint(9999, 99999)}',
                'product_type': MEDICINE,
                'active_substance': f'Medicine {number}',
                'dosage': f'Medicine {number}',
                'laboratory': f'Medicine {number}',
                'administration_way': f'Medicine {number}',
                'presentation_medicine': random.choice(presentations),
                'cost': self.faker.pydecimal(left_digits=3, right_digits=2, positive=True, min_value=10,
                                             max_value=10000),
                'application_cost': self.faker.pydecimal(left_digits=3, right_digits=2, positive=True, min_value=10,
                                                         max_value=10000)
            }
            medicines.append(Product.objects.create(**data))

    def create_stocks(self):
        warehouses = Warehouse.objects.all()[:2]
        products = Product.objects.all()

        for wh in warehouses:
            for number in range(0, 10):
                data = {
                    'quantity': random.randint(10, 100),
                    'warehouse': wh,
                    'product': random.choice(products)
                }
                try:
                    Stock.objects.create(**data)
                except Exception as error:
                    pass

    def create_providers(self):

        for p in range(0, 10):
            data = {'name': f'Proveedor {p}'}
            Provider.objects.create(**data)

    def handle(self, *args, **kwargs):
        self.create_groups()
        admin = self.create_users()

        with impersonate(admin):
            self.create_areas()
            self.create_types()
            self.create_spaces()

            self.create_warehouse()
            self.create_pesentation_medicine()
            self.create_products()
            self.create_medicine()
            self.create_stocks()
            self.create_providers()
