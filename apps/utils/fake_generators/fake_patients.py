# -*- coding: utf-8 -*-
import random

from faker import Faker

fake = Faker()
GENDERS = ['MALE', 'FEMALE']
CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'


def gen_random_gender():
    return random.choice(GENDERS)


def get_fake_curp():
    return ''.join(random.choices(CHARS, k=18))


def _gen_common_fields(gender=None):
    common_data = {
        # Common fields
        'name': fake.first_name_male() if gender == 'MALE' else fake.first_name_female(),
        'father_last_name': fake.last_name(),
        'mother_last_name': fake.last_name(),
        'phone': fake.phone_number(),

        'country': fake.country(),
        'state': fake.state(),
        'municipality': fake.city(),
        'location': fake.city(),
        'colony': fake.city(),
        'street': fake.street_name(),
        'ext_number': str(random.randint(10, 10000)),
        'int_number': str(random.randint(10, 10000)),
        'between_streets': f'Entre {fake.street_name()} y {fake.street_name()}',
    }
    return common_data


def _gen_patient_fields(gender=None):
    patient_data = {
        # Patient fields
        'curp': get_fake_curp(),
        'cel_phone': fake.phone_number(),
        'gender': gender,
        'dob': fake.date_this_century(before_today=True, after_today=False),
        'occupation': fake.job()
    }
    return patient_data


def get_fake_full_patient_data(sex=None):
    sex = sex if sex else gen_random_gender()
    patient_data = {}
    patient_data.update(_gen_common_fields(sex))
    patient_data.update(_gen_patient_fields(sex))
    return patient_data


def get_fake_min_patient_data(sex=None):
    sex = sex if sex else gen_random_gender()
    min_data = get_fake_full_patient_data(sex)
    values_to_remove = [
        'phone',
        'country',
        'state',
        'municipality',
        'location',
        'colony',
        'street',
        'ext_number',
        'int_number',
        'between_streets',
        'occupation',
    ]
    for value in values_to_remove:
        del min_data[value]
    return min_data


def get_fake_full_relative(sex=None):
    sex = sex if sex else gen_random_gender()
    relative_data = _gen_common_fields(sex)
    return relative_data


def get_fake_min_relative(sex=None):
    sex = sex if sex else gen_random_gender()
    relative_data = _gen_common_fields(sex)
    values_to_remove = [
        'phone',
        'country',
        'state',
        'municipality',
        'location',
        'colony',
        'street',
        'ext_number',
        'int_number',
        'between_streets',
    ]
    for value in values_to_remove:
        del relative_data[value]
    return relative_data
