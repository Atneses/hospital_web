# -*- coding: utf-8 -*-
import random

from faker import Faker

fake = Faker()
RELATIONS = [
    'Madre',
    'Padre',
    'Hermano/a',
    'Primo',
    'Tio/a',
    'Sobrino/a',
    'Amigo/a',
    'Vecino/a',
    'Conocido/a',
    'Otro/a'
]


def get_fake_full_hospitalization():
    hospitalization_data = {
        'relation': random.choice(RELATIONS),
        'state': fake.state(),
        'watch': fake.pybool(),
        'partial_payment': fake.pybool(),
        'space_cost': fake.pydecimal(left_digits=3, right_digits=2, positive=True, min_value=100, max_value=999)
    }
    return hospitalization_data
