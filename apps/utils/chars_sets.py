# -*- coding: utf-8 -*-
DATE_FORMAT_CODE = '%Y%m%d'
TIME_FORMAT_CODE = '%H%M'

DATE_FORMAT = '%Y/%m/%d'
TIME_FORMAT = '%H:%M:%S'
DATE_TIME_FORMAT = f'{DATE_FORMAT} {TIME_FORMAT}'

UPPER_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
LOWER_CHARS = 'abcdefghijklmnopqrstuvwxyz'
NUMBER = '0123456789'
SPECIAL_CHARS = r'\'!”#$%&’()*+,-./:;<=>?@[\]^_`{|}~'

ALL_CHARS = UPPER_CHARS + LOWER_CHARS + NUMBER + SPECIAL_CHARS
