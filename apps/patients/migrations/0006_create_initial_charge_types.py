from django.contrib.auth.hashers import make_password
from django.db import migrations


def create_initial_charges_types(apps, schema_editor):
    ChargeType = apps.get_model('patients', 'ChargeType')
    CustomUser = apps.get_model('profiles', 'CustomUser')
    try:
        user = CustomUser.objects.get(pk=1)
    except Exception as error:
        print(error)
        superuser_data = {
            'username': 'admin',
            'email': 'admin@admin.com',
            'password': make_password('admin1'),
            'is_active': True,
            'is_superuser': True
        }
        user = CustomUser.objects.create(**superuser_data)

    types = [
        {'id': 1, 'name': 'Honorario', 'created_by': user, 'modified_by': user},
        {'id': 2, 'name': 'Insumo', 'created_by': user, 'modified_by': user},
        {'id': 3, 'name': 'Medicamento', 'created_by': user, 'modified_by': user},
        {'id': 4, 'name': 'Otro', 'created_by': user, 'modified_by': user},
    ]
    for charge_type in types:
        print(f'Creating new charge type "{charge_type["name"]}"')
        ChargeType.objects.create(**charge_type)


def delete_initial_charges_types(apps, schema_editor):
    ids = [1, 2, 3, 4]
    ChargeType = apps.get_model('patients', 'ChargeType')
    charges_types = ChargeType.objects.filter(id__in=ids)
    charges_types.delete()


class Migration(migrations.Migration):
    dependencies = [
        ('patients', '0005_auto_20200822_2309'),
    ]

    operations = [
        migrations.RunPython(create_initial_charges_types, delete_initial_charges_types)
    ]
