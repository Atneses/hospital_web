# -*- coding: utf-8 -*-
import datetime
import logging
from copy import deepcopy
from io import BytesIO
from typing import Any

from django.contrib.auth import authenticate
from django.db import transaction
from django.db.models import QuerySet
from django.db.models.query_utils import Q
from django.http.response import HttpResponse
from django.template.loader import get_template
from django.utils import timezone
from rest_framework import status
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from xhtml2pdf import pisa

from apps.common.common_views import HospitalBaseViewSet, HospitalBaseListAPIView
from apps.common.groups_variables import WAREHOUSE_GROUP_ID
from apps.patients.models import Hospitalization, Payment, Discount, Charge, ChargeType, ENTRY, PRE_HIGH
from apps.patients.permissions.charges import CanPayHonorariums
from apps.patients.serializers import ChargeSerializer, ChargeTypeSerializer, PaymentSerializer, DiscountSerializer, \
    ChargeSmallSerializer
from apps.warehouse.models import Stock

logger = logging.getLogger(__name__)


# Create your views here
class HonorariumViewSet(HospitalBaseViewSet):
    permission_classes = [IsAuthenticated, CanPayHonorariums]
    queryset = Charge.objects.filter(charge_type=1, paid=None)
    serializer_class = ChargeSerializer
    pagination_class = LimitOffsetPagination

    def create(self, request, **kwargs):
        password = request.data['password']
        username = request.data['user']

        charges_ids = request.data['honorariums']
        physician = set(Charge.objects.filter(id__in=charges_ids).values_list('physician'))

        if len(physician) > 1:
            raise ValidationError("Can't pay honorariums for more than one physician at the same time")

        # Get physician user
        charge = Charge.objects.filter(id__in=charges_ids).first()
        if charge:
            user = charge.physician
            login = authenticate(username=username, password=password)
            print(login)
            if login and login == user:
                # Mark charges as paid
                honorariums = Charge.objects.filter(id__in=charges_ids, paid__isnull=True)
                result = honorariums.update(paid=timezone.now())
                return Response({'message': f'{result} honorariums paid'}, status=200)
            return Response({'message': 'Invalid username or password'}, status=status.HTTP_403_FORBIDDEN)
        else:
            return Response({'There is not left charge to paid'})

    def update(self, request, pk=None, **kwargs):
        response = {'message': 'Update function is not offered in this path.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)

    def partial_update(self, request, pk=None, **kwargs):
        response = {'message': 'Update function is not offered in this path.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, pk=None, **kwargs):
        response = {'message': 'Delete function is not offered in this path.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)


class ChargeViewSet(HospitalBaseViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Charge.objects.all()
    serializer_class = ChargeSerializer
    small_serializer_class = ChargeSmallSerializer
    pagination_class = LimitOffsetPagination

    def destroy(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        if request.user.has_perm('patients.delete_charge'):
            pk = kwargs['pk']
            charge = Charge.objects.get(pk=pk)
            if charge.product:
                Stock.add_to_stock(charge.product, charge.warehouse, charge.quantity)
            charge.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

        error = {'errors': [f'User: "{request.user}" doesn\'t have permission to delete a charge.']}
        raise ValidationError(error)

    def create(self, request, *args: Any, **kwargs: Any) -> Response:
        valid = request.user.check_password(request.data['password'])
        if valid:
            if request.user.is_superuser:
                request._full_data = request.data['charges']
                return super().create(request)

            # TODO: Refactor, improve validations using permissions
            group = request.user.groups.first()
            user_group_validation = True
            if not request.user.is_superuser:
                for ct in request.data['charges']:
                    charge_type_validation = (group.id in [1, 3, 5] and ct['charge_type'] in [1, 4]) \
                                             or (group.id == 4 and ct['charge_type'] in [2, 3]) \
                                             or (group.id == 1 and ct['charge_type'] in [])
                    user_group_validation = user_group_validation and charge_type_validation

            if user_group_validation:
                request._full_data = request.data['charges']
                return super().create(request)
            else:
                raise ValidationError({"charge_type": [
                    "This user group " + str(group['name']) + " is not allowed to create this kind of charge"]})
        else:
            raise ValidationError({"password": ["Incorrect password"]})

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)

        if self.request.user.is_warehouse_user:
            q = (Q(quantity__gt=0) & Q(charge_type_id__in=[2, 3]) &
                 (Q(hospitalization__state=ENTRY) | Q(hospitalization__state=PRE_HIGH)))
        else:
            q = (Q(quantity__gt=0) & Q(charge_type_id__in=[2, 3, 4])) | Q(charge_type_id=1)

        return queryset.filter(q)

    def partial_update(self, request, *args, **kwargs):
        if request.user.has_perm('patients.change_charge'):
            return super().partial_update(request, *args, **kwargs)
        else:
            raise ValidationError('Dont have permissions')


class ChargesSearch(HospitalBaseListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Charge.objects.all()
    serializer_class = ChargeSmallSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self) -> QuerySet:
        search = self.kwargs['search']

        q = Q(name__icontains=search) | \
            Q(created__contains=search) | \
            Q(hospitalization__patient__name__icontains=search) | \
            Q(hospitalization__patient__father_last_name__icontains=search) | \
            Q(hospitalization__patient__mother_last_name__icontains=search) | \
            Q(product__name__icontains=search) | \
            Q(product__brand__icontains=search) | \
            Q(product__barcode__icontains=search) | \
            Q(product__product_type__icontains=search) | \
            Q(product__presentation__icontains=search) | \
            Q(product__cost__icontains=search) | \
            Q(product__application_cost__icontains=search) | \
            Q(product__active_substance__icontains=search) | \
            Q(product__dosage__icontains=search) | \
            Q(created_by__username=search)
        results = Charge.objects.filter(q).all()

        if not self.request.user.is_superuser:
            results = results.filter(created_by__groups__id=WAREHOUSE_GROUP_ID)

        return results


class ChargeTypeViewSet(HospitalBaseViewSet):
    permission_classes = [IsAuthenticated]
    queryset = ChargeType.objects.all()
    serializer_class = ChargeTypeSerializer
    pagination_class = LimitOffsetPagination

    def create(self, request, **kwargs):
        response = {'message': 'Create function is not offered in this path.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)

    def update(self, request, pk=None, **kwargs):
        response = {'message': 'Update function is not offered in this path.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)

    def partial_update(self, request, pk=None, **kwargs):
        response = {'message': 'Update function is not offered in this path.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, pk=None, **kwargs):
        response = {'message': 'Delete function is not offered in this path.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)


class CreateRefundView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        try:
            with transaction.atomic():
                charge = Charge.objects.get(pk=kwargs['pk'])
                refund = deepcopy(charge)
                refund.id = None
                refund.is_refund = True
                refund.refund = charge
                refund.save()

                serializer = ChargeSerializer(refund)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Charge.DoesNotExist as error:
            return Response(status=status.HTTP_404_NOT_FOUND)


class PaymentViewSet(HospitalBaseViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    pagination_class = LimitOffsetPagination


class DiscountViewSet(HospitalBaseViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Discount.objects.all()
    serializer_class = DiscountSerializer
    pagination_class = LimitOffsetPagination


class Render:
    @staticmethod
    def render(path: str, params: dict):
        template = get_template(path)
        html = template.render(params)
        _file = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), _file)
        if not pdf.err:
            to_day = datetime.datetime.now().strftime('%H_%M_%S__%d_%m_%Y')
            filename = f'{params["hospitalization"].patient.get_name_for_file}_{to_day}.pdf'
            response = HttpResponse(_file.getvalue(), content_type='application/pdf')
            response['Content-Disposition'] = f'inline;filename={filename}'
            return response
        else:
            return HttpResponse('Error Rendering PDF', status=400)


class PaymentOrderPDF(APIView):
    def get(self, request, *args, **kwargs):
        try:
            hospitalization = Hospitalization.objects.get(pk=kwargs['id'])
            return Render.render('payment_order.html', locals())
        except Hospitalization.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
