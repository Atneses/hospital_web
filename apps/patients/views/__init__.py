# -*- coding: utf-8 -*-
from .charges import *
from .hospitalizations import *
from .patients import *
from .physicians import *
