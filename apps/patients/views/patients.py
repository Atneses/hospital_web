# -*- coding: utf-8 -*-
import logging
from typing import Any, Type

from django.db.models import Count, Q
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.generics import ListAPIView
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import BaseSerializer
from rest_framework.viewsets import ModelViewSet

from apps.patients.models import Document, ENTRY, PRE_HIGH
from apps.patients.models import Patient, Relative
from apps.patients.serializers import DocumentSerializer
from apps.patients.serializers import PatientShallowSerializer, PatientFullSerializer, RelativeSerializer, \
    PatientActiveHospitalizationSerializer

logger = logging.getLogger(__name__)


# Create your views here
class PatientViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Patient.objects.all()
    serializer_class = PatientShallowSerializer
    pagination_class = LimitOffsetPagination

    def destroy(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        try:
            patient = Patient.objects.get(pk=kwargs.get('pk', None))
            hospitalization = patient.hospitalization_set.all()
            hospitalization.delete()
        except Patient.DoesNotExist:
            pass
        return super().destroy(request, *args, **kwargs)

    def get_serializer_class(self) -> Type[BaseSerializer]:
        if self.action != 'list':
            return PatientFullSerializer
        return super().get_serializer_class()


class PatientActiveHospitalization(ListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Patient.objects.all()
    serializer_class = PatientActiveHospitalizationSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        is_active = True if self.kwargs['is_active'] == 1 else False
        queryset = super().get_queryset()
        active_hospitalizations = Q(hospitalization__state=ENTRY) | Q(hospitalization__state=PRE_HIGH)
        if is_active:
            queryset = queryset.annotate(
                count_active=Count(
                    'hospitalization',
                    filter=active_hospitalizations)
            ).filter(count_active__gt=0)
        else:
            queryset = queryset.annotate(
                count_active=Count(
                    'hospitalization',
                    filter=active_hospitalizations)
            ).filter(count_active=0)
        return queryset


class RelativeViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Relative.objects.all()
    serializer_class = RelativeSerializer
    pagination_class = LimitOffsetPagination


class DocumentViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    pagination_class = LimitOffsetPagination

    @action(detail=False, methods=['get'], url_path=r'patient/(?P<patientId>\w+)')
    def patient(self, request, patientId):
        if request.user.is_nurse:
            # For the nurse return only the most recent document
            document = Document.objects.filter(hospitalization__patient_id=patientId).order_by('-created').first()
            serialized_data = [DocumentSerializer(document).data]
            return Response(serialized_data)
        elif request.user.is_physician:
            return Response(Document.objects.filter(hospitalization__patient_id=patientId).values())
        return Response(Document.objects.filter(hospitalization__patient_id=patientId).values())
