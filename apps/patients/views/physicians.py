# -*- coding: utf-8 -*-
import logging

from django.db.models.query_utils import Q
from rest_framework.generics import ListAPIView
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated

from apps.patients.models import Hospitalization, PRE_HIGH, ENTRY
from apps.patients.serializers import PatientActiveHospitalizationSerializer

logger = logging.getLogger(__name__)


# Create your views here
class PhysicianPatientsList(ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = PatientActiveHospitalizationSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        keyword = self.request.GET.get('keyword')
        user = self.request.user
        query = Q(physicians=user) & (Q(state=PRE_HIGH) | Q(state=ENTRY))
        if keyword:
            query = query & (
                    Q(code__icontains=keyword)
                    | Q(relation__icontains=keyword)
                    | Q(patient__name__icontains=keyword)
                    | Q(patient__father_last_name__icontains=keyword)
                    | Q(patient__mother_last_name__icontains=keyword)
                    | Q(patient__phone__icontains=keyword)
                    | Q(patient__curp__icontains=keyword)
                    | Q(patient__cel_phone__icontains=keyword)
                    | Q(patient__occupation__icontains=keyword)
            )
        patients = [h.patient for h in Hospitalization.objects.filter(query).all()]
        return patients
