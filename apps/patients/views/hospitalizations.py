# -*- coding: utf-8 -*-
import logging

from django.db.models import Q
from django.db.models.aggregates import Count
from django.utils import timezone
from rest_framework.generics import ListAPIView
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from apps.common.common_views import HospitalBaseListAPIView
from apps.patients.models import Hospitalization, PRE_HIGH, ENTRY, Charge
from apps.patients.serializers import HospitalizationFullSerializer, HospitalizationSmallSerializer, \
    ChargeDebtorSerializer, HospitalizationPerDebtorSerializer

logger = logging.getLogger(__name__)


# Create your views here
class HospitalizationViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Hospitalization.objects.all()
    serializer_class = HospitalizationFullSerializer
    pagination_class = LimitOffsetPagination

    def partial_update(self, request, pk=None, **kwargs):
        data = request.data
        if 'data' in request.data:
            if request.data['state'] == ENTRY:
                data['end'] = None
            elif request.data['state'] == PRE_HIGH:
                data['end'] = timezone.now()

            request._full_data = data
        return super().partial_update(request)


class ActivePatientHospitalization(ListAPIView):
    """
    Hospitalization is active when:
    status ='PRE-HIGH' and partialPayment = False

    Note: In front end this is called Active Patients (Pacientes activos)
    """
    permission_classes = [IsAuthenticated]
    serializer_class = HospitalizationSmallSerializer
    pagination_class = LimitOffsetPagination
    query = Q(state=PRE_HIGH) & Q(partial_payment=False)
    queryset = Hospitalization.objects.filter(query).all()


class DebtorPatientHospitalization(ListAPIView):
    """
    Hospitalization is debtor when
    status = 'PRE-HIGH' and partialPayment = True

    Note: In front end this is called Debtor Patients (Pacientes deudores)
    """
    permission_classes = [IsAuthenticated]
    serializer_class = HospitalizationSmallSerializer
    pagination_class = LimitOffsetPagination
    query = Q(state=PRE_HIGH) & Q(partial_payment=True)
    queryset = Hospitalization.objects.filter(query).all()

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class DebtorPatientPerPhysician(HospitalBaseListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Hospitalization.objects.all()
    serializer_class = HospitalizationPerDebtorSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        user = self.request.user
        q = Q(charge__charge_type__id=1, charge__physician=user, charge__paid__isnull=True)
        query = Hospitalization.objects.annotate(charge_count=Count('charge', filter=q)).filter(charge_count__gt=0)
        return query


class ChargePerPhysicianPerHospitalization(HospitalBaseListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Charge.objects.all()
    serializer_class = ChargeDebtorSerializer
    pagination_class = LimitOffsetPagination

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super().get_queryset()

        hospitalization_id = self.kwargs.get('hospitalization_id')
        q = Q(physician=self.request.user) & Q(hospitalization_id=hospitalization_id) & Q(paid__isnull=True)
        queryset = queryset.filter(q)
        return queryset
