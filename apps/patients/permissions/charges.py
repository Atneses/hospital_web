# -*- coding: utf-8 -*-
from django.contrib.auth.models import Group
from rest_framework.permissions import BasePermission

from apps.common.groups_variables import RECEPTION_GROUP_ID, NURSING_GROUP_ID, CHECKOUT_GROUP_ID


class CanPayHonorariums(BasePermission):
    """
    Permission class to allow only a superuser, or a user of in any of the groups, Recepción or caja to pay honorariums.
    """
    def has_permission(self, request, view):
        user = request.user

        if user.is_superuser:
            return True

        groups = Group.objects.filter(id__in=[NURSING_GROUP_ID, RECEPTION_GROUP_ID, CHECKOUT_GROUP_ID]).all()
        for group in user.groups.all():
            if group in groups:
                return True
        return False
