from django.contrib import admin
from safedelete.admin import SafeDeleteAdmin

from apps.common.common_models import BASE_FIELDS
from apps.patients.models import Patient, Relative, Hospitalization, ChargeOld, Payment, Discount, ChargeType, Charge, \
    Document

# Register your models here.
COMMON_SEARCH_FIELDS = [
    'name',
    'father_last_name',
    'mother_last_name',
    'phone'
]


@admin.register(Patient)
class PatientAdmin(SafeDeleteAdmin):
    list_display = [
        'curp',
        'name',
        'father_last_name',
        'mother_last_name',
        'age',
        'has_active_hospitalization',
        'deleted'
    ]
    search_fields = COMMON_SEARCH_FIELDS + ['curp']
    readonly_fields = ['age', 'has_active_hospitalization'] + BASE_FIELDS


@admin.register(Relative)
class RelativeAdmin(SafeDeleteAdmin):
    list_display = [
        'name',
        'father_last_name',
        'mother_last_name',
        'deleted'
    ]
    search_fields = COMMON_SEARCH_FIELDS
    readonly_fields = BASE_FIELDS


class HospitalizationChargeInline(admin.TabularInline):
    model = Charge
    extra = 0
    readonly_fields = [
        'id',
        'subtotal',
        'name',
        'description',
        'is_refund',
        'paid',
        'cost',
        'application_cost',
        'quantity',
        'hospitalization',
        'charge_type',
        'product',
        'warehouse',
        'physician',
    ]
    exclude = ['created_by', 'modified_by']


@admin.register(Hospitalization)
class HospitalizationAdmin(SafeDeleteAdmin):
    list_display = ['id', 'code', 'state', 'patient', 'relative', 'relation', 'deleted']
    filter_horizontal = ('physicians',)
    list_filter = SafeDeleteAdmin.list_filter + ('state',)
    readonly_fields = ['code', 'space_cost_by_day', 'days_since_entry', 'days_since_pre_high'] + BASE_FIELDS
    search_fields = [
        'id',
        'code',
        'state',
        'patient__name',
        'patient__father_last_name',
        'patient__mother_last_name'
    ]
    inlines = [HospitalizationChargeInline]


@admin.register(ChargeOld)
class ChargesOldAdmin(SafeDeleteAdmin):
    list_display = ['id', 'total', 'name', 'hospitalization', 'deleted']
    search_fields = ['total', 'name', 'description', 'honorarium', 'hospitalization__code']
    readonly_fields = BASE_FIELDS


@admin.register(Charge)
class ChargeAdmin(SafeDeleteAdmin):
    list_display = ['id', 'subtotal', 'name', 'hospitalization', 'is_refund', 'charge_type', 'deleted', 'created']
    list_filter = ('charge_type__name', 'is_refund') + SafeDeleteAdmin.list_filter
    search_fields = ['id', 'subtotal', 'name', 'is_refund', 'deleted']
    ordering = ['-created']
    raw_id_fields = ['refund']
    readonly_fields = BASE_FIELDS + ['total']


@admin.register(ChargeType)
class ChargeTypeAdmin(SafeDeleteAdmin):
    list_display = ['id', 'name'] + BASE_FIELDS
    search_fields = ['name']
    readonly_fields = BASE_FIELDS


@admin.register(Payment)
class PaymentAdmin(SafeDeleteAdmin):
    list_display = ['amount', 'hospitalization', 'deleted']
    list_filter = () + SafeDeleteAdmin.list_filter
    search_fields = ['amount', 'hospitalization__code']
    readonly_fields = BASE_FIELDS


@admin.register(Discount)
class DiscountAdmin(SafeDeleteAdmin):
    list_display = ['name', 'percentage', 'amount', 'hospitalization', 'deleted']
    search_fields = ['name', 'percentage', 'amount', 'hospitalization__code']
    readonly_fields = BASE_FIELDS


@admin.register(Document)
class DocumentAdmin(SafeDeleteAdmin):
    list_display = ['hospitalization_id', 'hospitalization', 'patient']

    def patient(self, obj: Document):
        return obj.hospitalization.patient

    def hospitalization_id(self, obj: Document):
        return obj.hospitalization.id
