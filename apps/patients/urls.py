# -*- coding: utf-8 -*-
from django.urls import path, include
from rest_framework import routers

from apps.patients import views
from apps.patients.views import PaymentOrderPDF, PatientActiveHospitalization, ActivePatientHospitalization, \
    DebtorPatientHospitalization, DebtorPatientPerPhysician, PhysicianPatientsList, ChargePerPhysicianPerHospitalization

router = routers.DefaultRouter()
router.register('patients', views.PatientViewSet)
router.register('relatives', views.RelativeViewSet)
router.register('hospitalizations', views.HospitalizationViewSet)
router.register('charges', views.ChargeViewSet)
router.register('honorariums', views.HonorariumViewSet)
router.register('charge_types', views.ChargeTypeViewSet)
router.register('payments', views.PaymentViewSet)
router.register('discounts', views.DiscountViewSet)
router.register('documents', views.DocumentViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('patients-active-hospitalization/<int:is_active>',
         PatientActiveHospitalization.as_view(),
         name='patients-active-hospitalization'
         ),
    path('patients-active/', ActivePatientHospitalization.as_view(), name='patients-active'),
    path('patients-debtor/', DebtorPatientHospitalization.as_view(), name='patients-debtor'),
    path('patients-debtor/per-physician/', DebtorPatientPerPhysician.as_view(), name='patients-debtor-per-physician'),
    path(
        'patients-debtor/per-physician/<int:hospitalization_id>/',
        ChargePerPhysicianPerHospitalization.as_view(),
        name='patients-debtor-per-physician-per-hospitalization'
    ),
    path('payment-order/<int:id>', PaymentOrderPDF.as_view(), name='payment-order'),
    path('my-patients/', PhysicianPatientsList.as_view(), name='my-patients'),
    path('charges-search/<str:search>', views.ChargesSearch.as_view(), name='charge-search'),
    path('create-refund/<int:pk>/', views.CreateRefundView.as_view(), name='crate-refund'),
]
