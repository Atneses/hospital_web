# -*- coding: utf-8 -*-
from django.db.models.signals import post_save
from django.dispatch import receiver
from safedelete.signals import post_softdelete

from apps.patients.models import Charge
from apps.warehouse.models import Stock


@receiver(post_save, sender=Charge)
def update_charge(sender, instance: Charge, created, *args, **kwargs):
    if not created:
        if instance.old_quantity != instance.quantity:
            diff = instance.old_quantity - instance.quantity
            print(diff)
            if diff < 0:
                # Discount from stock
                Stock.subtract_from_stock(instance.product, instance.warehouse, diff * - 1)
            elif diff > 0:
                # Add to stock
                Stock.add_to_stock(instance.product, instance.warehouse, diff)
    elif instance.is_refund:
        # Add to stock
        Stock.add_to_stock(instance.product, instance.warehouse, instance.quantity)


@receiver(post_softdelete, sender=Charge)
def delete_charge(sender, instance: Charge, *args, **kwargs):
    print(instance.is_refund)
    if instance.is_refund:
        Stock.subtract_from_stock(instance.product, instance.warehouse, instance.quantity)
