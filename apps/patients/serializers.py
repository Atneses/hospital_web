# -*- coding: utf-8 -*-
from typing import Any

from django.db.models import Q
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField, empty
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import ModelSerializer

from apps.common.common_serializers import HospitalBaseSerializer, CustomUserShortSerializer
from apps.patients.models import Patient, Relative, Hospitalization, ENTRY, PRE_HIGH, Payment, Discount, Charge, \
    ChargeType, Document
from apps.profiles.models import CustomUser
from apps.profiles.serializers import CustomUserSerializer
from apps.spaces.models import Space
from apps.spaces.serializers import SpaceSerializer
from apps.warehouse.models import Product, Warehouse, Stock
from apps.warehouse.serializers import GenericProductSerializer, WarehouseSerializerForStock, ProductShallowSerializer


# region Patient Serializers
class PatientFullSerializer(HospitalBaseSerializer):
    has_active_hospitalization = serializers.BooleanField(read_only=True)
    is_debtor = serializers.BooleanField(read_only=True)
    has_document = serializers.BooleanField(read_only=True)
    full_name = serializers.CharField(required=False)

    class Meta(HospitalBaseSerializer.Meta):
        model = Patient


class PatientShallowSerializer(ModelSerializer):
    has_active_hospitalization = serializers.BooleanField(read_only=True)
    is_debtor = serializers.BooleanField(read_only=True)
    full_name = serializers.CharField(required=False)

    class Meta:
        model = Patient
        fields = '__all__'


class PatientSmallSerializer(ModelSerializer):
    class Meta:
        model = Patient
        fields = ['id', 'name', 'father_last_name', 'mother_last_name', 'curp']


class PatientActiveHospitalizationSerializer(ModelSerializer):
    has_document = serializers.BooleanField(read_only=True)

    class Meta:
        model = Patient
        fields = ['id', 'curp', 'name', 'father_last_name', 'mother_last_name', 'has_document']

    def to_representation(self, instance: Any) -> Any:
        # TODO: check this filter apps/patients/views/physicians.py PhysicianPatientsList
        data = super().to_representation(instance)
        q = Q(state=PRE_HIGH) | Q(state=ENTRY)
        h = instance.hospitalization_set.filter(q).first()
        if h:
            data['hospitalization'] = {
                'id': h.id,
                'state': h.state,
                'space_name': h.space.space_name if h.space else None
            }
        else:
            data['hospitalization'] = None

        return data


# endregion


class RelativeSerializer(HospitalBaseSerializer):
    class Meta(HospitalBaseSerializer.Meta):
        model = Relative


class ChargeTypeSerializer(ModelSerializer):
    class Meta(HospitalBaseSerializer.Meta):
        model = ChargeType
        depth = 1


class ChargeSerializer(HospitalBaseSerializer):
    hospitalization = PrimaryKeyRelatedField(queryset=Hospitalization.objects.all())
    product = PrimaryKeyRelatedField(queryset=Product.objects.all(), required=False)
    warehouse = PrimaryKeyRelatedField(queryset=Warehouse.objects.all(), required=False)
    physician = PrimaryKeyRelatedField(queryset=CustomUser.objects.all(), required=False)
    charge_type = PrimaryKeyRelatedField(queryset=ChargeType.objects.all())
    refund = PrimaryKeyRelatedField(queryset=Charge.objects.all(), required=False)
    charge = PrimaryKeyRelatedField(queryset=Charge.objects.all(), required=False)

    class Meta(HospitalBaseSerializer.Meta):
        model = Charge
        depth = 1

    def __init__(self, instance=None, data=empty, **kwargs):
        # TODO: Try to find a better way to make validations in serializer and in Django admin, both at the same time.
        if isinstance(data, dict) and 'quantity' in data:
            # Save quantity to validate when update a charge if is necessary to return or discount from stock.
            self.old_quantity = instance.quantity
            self.new_quantity = data['quantity']
        super().__init__(instance, data, **kwargs)

    def create(self, validated_data: Any) -> Any:
        data = super().create(validated_data)

        is_refund = validated_data.get('is_refund', None)
        warehouse = validated_data.get('warehouse', None)
        product = validated_data.get('product', None)
        quantity = validated_data.get('quantity', None)
        medic = validated_data.get('medic', None)

        if warehouse and product and quantity and not is_refund:
            # Discount from stock
            Stock.subtract_from_stock(product, warehouse, quantity)
        return data

    def update(self, instance: Charge, validated_data, *args, **kwargs):
        if 'quantity' in validated_data:
            diff = self.old_quantity - self.new_quantity
            if diff < 0:
                errors = {}
                errors.update(self.stock_validate(instance.warehouse, instance.product, diff * -1))
                if len(errors) > 0:
                    raise ValidationError(errors)
            elif diff > 0:
                pass

        return super().update(instance, validated_data)

    def validate(self, attrs):
        is_refund = attrs.get('is_refund', None)
        product = attrs.get('product', None)
        warehouse = attrs.get('warehouse', None)
        quantity = attrs.get('quantity', None)
        physician = attrs.get('physician', None)
        charge_type = attrs.get('charge_type', None)

        if charge_type:
            errors = {}
            if charge_type.id == 1:
                if not physician:
                    errors['physician'] = ['If the charge it\'s honorarium charge, a Physician is required']
                if product:
                    errors['product'] = ['If the charge it\'s honorarium charge, you cannot specify a Product']
                if warehouse:
                    errors['warehouse'] = ['If the charge it\'s honorarium charge, you cannot specify a Warehouse']
                if len(errors) > 0:
                    raise ValidationError(errors)
            elif charge_type.id in [2, 3]:
                if not product:
                    errors['product'] = ['If the charge it\'s supplies charge, a Product is required']
                if not warehouse:
                    errors['warehouse'] = ['If the charge it\'s supplies charge, a Warehouse is required']
                if not quantity:
                    errors['quantity'] = ['If the charge it\'s supplies charge, a quantity greater than 0 is required']
                if physician:
                    errors['physician'] = ['If the charge it\'s supplies charge, you cannot specify a physician']
                errors.update(self.product_validate(warehouse, product, quantity))
                errors.update(self.stock_validate(warehouse, product, quantity))
                if len(errors) > 0:
                    raise ValidationError(errors)
            if is_refund:
                attrs['total'] *= -1

        return attrs

    def product_validate(self, warehouse, product, quantity):
        # region Validation when product is sent it
        errors = {}
        if product and not warehouse:
            errors['warehouse'] = ['This field must be specified when product is sent it']

        if warehouse and not product:
            errors['product'] = ['This field must be specified when warehouse is sent it']

        if (warehouse or product) and not quantity:
            errors['quantity'] = ['This field is required when product is sent it']

        if quantity and not (warehouse or not product):
            errors['product'] = ['This field must be specified when quantity is sent it']
            errors['warehouse'] = ['This field must be specified when quantity is sent it']
        return errors
        # endregion

    def stock_validate(self, warehouse, product, quantity):
        # region Validate stock
        errors = {}
        q = Q(warehouse=warehouse) & Q(product=product)
        stock = Stock.objects.filter(q).first()
        if warehouse and product and quantity:
            if stock:
                if stock.quantity <= 0:
                    errors['stock'] = [
                        f'Stock for product: "{product}" in warehouse: "{warehouse}" is {stock.quantity}']
                elif stock.quantity < quantity:
                    errors['stock'] = [f'Not enough stock for product "{product}", available stock: {stock.quantity}']
            else:
                errors['stock'] = [f'Not available stock for product: "{product}" in warehouse: "{warehouse}"']
        return errors
        # endregion

    def medic_validate(self):
        # Validate charge type and medic existence if applicable
        pass

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['charge_type'] = ChargeTypeSerializer(instance.charge_type).data
        data['product'] = GenericProductSerializer(instance.product).data
        data['warehouse'] = WarehouseSerializerForStock(instance.warehouse).data
        data['physician'] = CustomUserSerializer(instance.physician, many=False).data
        data['hospitalization'] = HospitalizationSmallSerializer(instance.hospitalization, many=False).data
        return data


class ChargeSmallSerializer(ModelSerializer):
    charge = PrimaryKeyRelatedField(queryset=Charge.objects.all(), required=False)

    class Meta:
        model = Charge
        # TODO: Add warehouse to the return response when multiple warehouses are in use.
        exclude = ['hospitalization', 'product', 'warehouse', 'physician']

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['created_by'] = CustomUserShortSerializer(instance.created_by).data
        data['product'] = ProductShallowSerializer(instance.product).data
        data['hospitalization'] = HospitalizationSmallSerializer(instance.hospitalization).data
        return data


class ChargeDebtorSerializer(ModelSerializer):
    class Meta:
        model = Charge
        exclude = ['hospitalization', 'product', 'warehouse', 'physician']

    def to_representation(self, instance: Charge):
        data = super().to_representation(instance)
        # data['hospitalization'] = HospitalizationPerChargeSerializer(instance.hospitalization).data
        return data


# region Hospitalization Serializers
class HospitalizationFullSerializer(HospitalBaseSerializer):
    can_pay_honorariums = serializers.BooleanField(read_only=True)
    charge_set = PrimaryKeyRelatedField(many=True, read_only=True)
    checkout_total = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)
    code = CharField(max_length=41, read_only=True)
    discount_set = PrimaryKeyRelatedField(many=True, read_only=True)
    end = serializers.DateTimeField(required=False)
    patient = PrimaryKeyRelatedField(queryset=Patient.objects.all())
    payment_set = PrimaryKeyRelatedField(many=True, read_only=True)
    physicians = PrimaryKeyRelatedField(
        queryset=CustomUser.objects.all(),
        required=False,
        allow_null=True,
        many=True
    )
    relative = PrimaryKeyRelatedField(queryset=Relative.objects.all(), required=False, allow_null=True)
    space = PrimaryKeyRelatedField(queryset=Space.objects.all(), required=False, allow_null=True)
    space_cost_by_day = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)

    class Meta(HospitalBaseSerializer.Meta):
        model = Hospitalization

    def validate(self, attrs):
        patient = attrs.get('patient', None)
        if patient:
            q = Q(state=ENTRY) | Q(state=PRE_HIGH)
            active = patient.hospitalization_set.filter(q).all()
            if len(active) > 0:
                errors = {
                    'hospitalization': [f'There is already are an active hospitalization for this patient: {patient}']
                }
                raise serializers.ValidationError(errors)
        state = attrs.get('state', None)
        space = attrs.get('space', None)
        self.space_validate(state, space)
        self.space_unique_validate(space)
        return super().validate(attrs)

    def space_validate(self, state, space):
        if state == ENTRY and not space:
            errors = {
                'errors': [f'Hospitalization must have an space if the state is "{ENTRY}"']
            }
            raise serializers.ValidationError(errors)

    def space_unique_validate(self, space):
        if space:
            h = Hospitalization.objects.filter(space=space).all()
            if len(h) > 0:
                errors = {
                    'errors': [f'There is already an Hospitalization assigned to space "{space}"']
                }
                raise serializers.ValidationError(errors)

    def validate_physicians(self, physicians):
        for user in physicians:
            group = user.groups.filter(id=1).first()
            if not group:
                errors = {
                    'errors': [f'The user "{user}" is not part of the physicians group.']
                }
                raise serializers.ValidationError(errors)
        return physicians

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['patient'] = PatientFullSerializer(instance.patient).data
        if instance.relative:
            data['relative'] = RelativeSerializer(instance.relative).data
        else:
            data['relative'] = None
        data['charge_set'] = ChargeSerializer(instance.charge_set, many=True).data
        data['payment_set'] = PaymentSerializer(instance.payment_set, many=True).data
        if instance.space:
            data['space'] = SpaceSerializer(instance.space).data
        else:
            data['space'] = None
        data['discount_set'] = DiscountSerializer(instance.discount_set, many=True).data
        if instance.physicians:
            data['physicians'] = CustomUserSerializer(instance.physicians, many=True).data
        return data


class HospitalizationSmallSerializer(ModelSerializer):
    class Meta:
        model = Hospitalization
        fields = ['id']

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['patient'] = PatientSmallSerializer(instance.patient).data
        return data


class HospitalizationPerDebtorSerializer(ModelSerializer):
    can_pay_honorariums = serializers.BooleanField()
    hospitalization_code = serializers.CharField(max_length=100, source='code')
    hospitalization_state = serializers.CharField(max_length=100, source='state')

    class Meta:
        model = Hospitalization
        fields = ['id', 'hospitalization_code', 'can_pay_honorariums', 'hospitalization_state']

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['name'] = instance.patient.name
        data['father_last_name'] = instance.patient.father_last_name
        data['mother_last_name'] = instance.patient.mother_last_name
        data['curp'] = instance.patient.curp
        data['honorariums_count'] = instance.charge_count
        return data


class HospitalizationPerChargeSerializer(ModelSerializer):
    can_pay_honorariums = serializers.BooleanField()
    hospitalization_code = serializers.CharField(max_length=100, source='code')
    hospitalization_state = serializers.CharField(max_length=100, source='state')

    class Meta:
        model = Hospitalization
        fields = ['id', 'hospitalization_code', 'can_pay_honorariums', 'hospitalization_state']

    def to_representation(self, instance: Hospitalization):
        data = super().to_representation(instance)
        data['name'] = instance.patient.name
        data['father_last_name'] = instance.patient.father_last_name
        data['mother_last_name'] = instance.patient.mother_last_name
        data['curp'] = instance.patient.curp
        data['honorariums_count'] = instance.charge_set.filter(charge_type__id=1).count()
        return data


# endregion


class PaymentSerializer(HospitalBaseSerializer):
    hospitalization = PrimaryKeyRelatedField(queryset=Hospitalization.objects.all())

    class Meta(HospitalBaseSerializer.Meta):
        model = Payment


class DiscountSerializer(HospitalBaseSerializer):
    hospitalization = PrimaryKeyRelatedField(queryset=Hospitalization.objects.all())

    class Meta(HospitalBaseSerializer.Meta):
        model = Discount

    def validate(self, attrs):
        percentage = attrs.get('percentage', None)
        amount = attrs.get('amount', None)

        if 'percentage' in attrs or 'amount' in attrs:
            self.validate_percentage_amount(percentage, amount)

        if not amount and percentage:
            attrs['amount'] = None
        if not percentage and amount:
            attrs['percentage'] = None

        return super().validate(attrs)

    def validate_percentage_amount(self, percentage, amount):
        if percentage and amount:
            errors = {
                'errors': ['A discount can only have "percentage" or "amount" not both at the same time.']
            }
            raise serializers.ValidationError(errors)
        if not percentage and not amount:
            errors = {
                'error': ['You must specify a "percentage" or an "amount".']
            }
            raise serializers.ValidationError(errors)


class DocumentSerializer(HospitalBaseSerializer):
    hospitalization = PrimaryKeyRelatedField(queryset=Hospitalization.objects.all())

    class Meta(HospitalBaseSerializer.Meta):
        model = Document
