from collections import OrderedDict

from crum import impersonate
from django.urls import reverse
from rest_framework import status

from apps.common.common_tests import CustomBaseTest
from apps.patients.models import Patient
from apps.utils.fake_generators.fake_hospitalization import get_fake_full_hospitalization
from apps.utils.fake_generators.fake_patients import get_fake_min_patient_data, get_fake_full_patient_data


# Create your tests here.
class HospitalizationTest(CustomBaseTest):

    def test_create(self):
        url = reverse('patient:hospitalization-list')
        data = get_fake_full_hospitalization()
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        pass

    def test_patch(self):
        pass

    def test_delete(self):
        pass


class PatientTest(CustomBaseTest):

    def setUp(self) -> None:
        super().setUp()
        self.create_initial_patients()

    # region CRUD methods
    def create_initial_patients(self):
        with impersonate(self.admin):
            for patient in range(0, 2):
                Patient.objects.create(**get_fake_full_patient_data())

    def test_create_min_patient(self):
        url = reverse('patient:patient-list')
        data = get_fake_min_patient_data()
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_full_patient(self):
        url = reverse('patient:patient-list')
        data = get_fake_full_patient_data()
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_patient(self):
        url = reverse('patient:patient-detail', args=[1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('id', response.data)

    def test_update_patient(self):
        url = reverse('patient:patient-detail', args=[1])
        data = {'name': 'test update'}
        response = self.client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], data['name'])

    def test_patient_list(self):
        url = reverse('patient:patient-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response.data, OrderedDict)

    def test_delete_patient(self):
        url = reverse('patient:patient-detail', args=[1])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    # endregion

    # region Testing properties
    def test_get_patient_age(self):
        patient = Patient.objects.first()
        self.assertIsInstance(patient.age, int)
        self.assertGreater(patient.age, 0)
    # endregion
