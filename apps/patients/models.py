import random
from datetime import date, datetime

from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator, MaxLengthValidator, MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import Q, Sum
from django.utils import timezone
from django.utils.text import slugify
from django.utils.timezone import make_aware

from apps.common.common_models import HospitalBaseModel
from apps.utils.chars_sets import ALL_CHARS, TIME_FORMAT_CODE, DATE_FORMAT_CODE
from apps.warehouse.models import Stock, Product

MALE = 'MALE'
FEMALE = 'FEMALE'
GENDER_CHOICES = ((MALE, 'Masculino'), (FEMALE, 'Femenino'))

ENTRY = 'ENTRY'
PRE_HIGH = 'PRE-HIGH'
HIGH = 'HIGH'
HOSPITALIZATION_STATES_CHOICES = (
    (ENTRY, 'Ingreso'),
    (PRE_HIGH, 'Pre-Alta'),
    (HIGH, 'Alta')
)
PERCENTAGE_ERROR_MESSAGE = 'Valid values are between 0 and 100'


# Create your models here.
class PatientBaseModel(HospitalBaseModel):
    """
    Abstract class for patients and relatives.
    """
    # region Personal
    name = models.CharField(max_length=100)
    father_last_name = models.CharField(max_length=100)
    mother_last_name = models.CharField(max_length=100)
    phone = models.CharField(max_length=30, blank=True, null=True)
    # endregion

    # region Address
    country = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=100, blank=True, null=True)
    municipality = models.CharField(max_length=100, blank=True, null=True)
    location = models.CharField(max_length=100, blank=True, null=True)
    colony = models.CharField(max_length=100, blank=True, null=True)
    street = models.CharField(max_length=100, blank=True, null=True)
    ext_number = models.CharField(max_length=50, blank=True, null=True)
    int_number = models.CharField(max_length=50, blank=True, null=True)
    between_streets = models.CharField(max_length=100, blank=True, null=True)

    # endregion

    class Meta:
        abstract = True

    def __str__(self):
        return f'{self.name} {self.father_last_name} {self.mother_last_name}'

    @property
    def full_name(self):
        name = f'{self.name} {self.father_last_name} {self.mother_last_name}'
        return name.strip()

    @property
    def get_name_for_file(self):
        name = slugify(self.full_name)
        return name.strip()


class Patient(PatientBaseModel):
    curp = models.CharField(
        max_length=18,
        unique=True,
        validators=[MaxLengthValidator(limit_value=18), MinLengthValidator(limit_value=18)]
    )
    cel_phone = models.CharField(max_length=100)
    gender = models.CharField(
        max_length=20,
        choices=GENDER_CHOICES,
        help_text=f'Available choices are {MALE} and {FEMALE}'
    )
    dob = models.DateField()
    occupation = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        ordering = ['name', 'father_last_name', 'mother_last_name']

    @property
    def age(self):
        today = date.today()
        age = today.year - self.dob.year - ((today.month, today.day) < (self.dob.month, self.dob.day))
        return age

    @property
    def has_active_hospitalization(self):
        """
        Return True if the patient had an Hospitalization in state ENTRY or PRE_HIGH
        otherwise return False
        :return: Boolean
        """
        q_entry = Q(state=ENTRY)
        q_pre_high = Q(state=PRE_HIGH)
        entry = self.hospitalization_set.filter(q_entry).count()
        pre_high = self.hospitalization_set.filter(q_pre_high).count()

        if entry > 0:
            return True
        elif pre_high > 0 and not self.is_debtor:
            return True
        elif pre_high > 0 and self.is_debtor:
            return False
        return False

    @property
    def is_debtor(self):
        q = Q(partial_payment=True)
        h = self.hospitalization_set.filter(q).count()
        return h > 0

    @property
    def has_document(self):
        document_count = Document.objects.filter(hospitalization__patient=self).count()
        return document_count > 0


class Relative(PatientBaseModel):
    class Meta:
        pass


class Hospitalization(HospitalBaseModel):
    code = models.CharField(max_length=41, unique=True)
    pre_high_date = models.DateTimeField(blank=True, null=True)
    high_date = models.DateTimeField(blank=True, null=True)
    relation = models.CharField(max_length=200, blank=True, null=True)
    state = models.CharField(
        max_length=20,
        choices=HOSPITALIZATION_STATES_CHOICES,
        help_text=f'Available choices are: {ENTRY}, {PRE_HIGH} and {HIGH}'
    )
    watch = models.BooleanField(
        default=False,
        help_text='It indicate if the patient need constant surveillance'
    )
    partial_payment = models.BooleanField(default=False)
    space_cost = models.DecimalField(max_digits=10, decimal_places=2)

    # Relationships
    patient = models.ForeignKey('patients.Patient', on_delete=models.CASCADE)
    relative = models.ForeignKey('patients.Relative', on_delete=models.CASCADE, blank=True, null=True)
    space = models.OneToOneField('spaces.Space', on_delete=models.CASCADE, blank=True, null=True)
    physicians = models.ManyToManyField(
        'profiles.CustomUser',
        related_name='hospitalizations',
        limit_choices_to=Q(groups__id=1),
        null=True,
        blank=True
    )

    class Meta:
        pass

    def __str__(self):
        return f'{self.code}'

    def save(self, keep_deleted=False, **kwargs):
        if not self.code:
            self.code = self.gen_code()
        if self.state == PRE_HIGH or self.state == HIGH:
            self.space = None

        # Save PRE-HIGH date
        if self.state == PRE_HIGH:
            self.pre_high_date = timezone.now()

        # Save HIGH date
        if self.state == HIGH:
            self._remove_physicians()
            self.high_date = timezone.now()

        super().save(keep_deleted, **kwargs)

    def clean(self) -> None:
        """
        Validate if the Patient has an Hospitalization is ENTRY or PRE HIGH state
        otherwise throws a Validation error, Patient can only had one active
        hospitalization at time.
        :return:
        """
        if hasattr(self, 'patient'):
            if self.patient.has_active_hospitalization and not self.id:
                raise ValidationError(
                    f'There is already are an active hospitalization for this patient: {self.patient}'
                )
        if self.state == ENTRY and not self.space:
            raise ValidationError(f'Hospitalization must have an space if the state is {ENTRY}')

        super().clean()

    def gen_code(self):
        """
        Generate a random code unique for every Hospitalization with the next
        format: <date>-<patients_curp>-<hour>-<8_random_chars>
        date in the next format: YYYYmmdd
        hour in the next format: HHMM
        :return:
        """
        _date = datetime.now()
        today = _date.date().strftime(DATE_FORMAT_CODE)
        _time = _date.time().strftime(TIME_FORMAT_CODE)
        chars = ''.join(random.choices(ALL_CHARS, k=8))
        code = f'{today}-{self.patient.curp}-{_time}-{chars}'
        return code

    def _get_charges_total(self, is_refund=False):
        charges = self.charge_set.all()
        total = sum([charge.total() for charge in charges if charge.is_refund is is_refund])
        return total

    def _remove_physicians(self):
        """
        Remove all physicians from the hospitalization
        :return:
        """
        self.physicians.clear()

    # region Hospitalization calculations
    @property
    def charge_total(self):
        total = self.charge_total_without_space + self.space_cost_by_day
        return total

    @property
    def charge_total_without_space(self):
        total = sum([charge.total() for charge in self.charge_set.all() if not charge.is_refund])
        return total

    @property
    def checkout_total(self):
        charges = self._get_charges_total()
        charges = charges if charges is not None else 0

        refunds = self._get_charges_total(True)
        refunds = refunds if refunds is not None else 0

        discounts = self.discount_set.filter(amount__gte=0).aggregate(total=Sum('amount'))['total']
        discounts = discounts if discounts is not None else 0

        payments = self.payment_set.aggregate(total=Sum('amount'))['total']
        payments = payments if payments is not None else 0

        hospitalization_total = (charges + self.space_cost_by_day) - refunds - discounts - payments
        return hospitalization_total

    @property
    def discounts_amount_total(self):
        total = sum([discount.amount for discount in self.discount_set.all() if discount.amount])
        return total

    @property
    def honorariums_charge(self):
        q = Q(charge__charge_type_id=1)
        hospitalization = Hospitalization.objects.annotate(
            hospital_charges_sum=Sum('charge__subtotal', filter=q)
        ).filter(id=self.id).first()

        return hospitalization.hospital_charges_sum

    @property
    def hospital_charges(self):
        """
        Return the total sum of the charges that belong to charge types 'Otro', 'Medicamento', and 'Insumo'
        :return: int
        """
        q = ~Q(charge__charge_type_id=1)
        hospitalization = Hospitalization.objects.annotate(
            hospital_charges_sum=Sum('charge__subtotal', filter=q)
        ).filter(id=self.id).first()
        if hospitalization.hospital_charges_sum:
            return hospitalization.hospital_charges_sum
        return 0

    @property
    def hospitalization_total(self):
        total = (self.charge_total_without_space + self.space_cost_by_day) \
                - self.refund_total - self.discounts_amount_total
        return total

    @property
    def payments_total(self):
        total = sum([payment.amount for payment in self.payment_set.all()])
        return total

    @property
    def refund_total(self):
        total = sum([charge.total() for charge in self.charge_set.all() if charge.is_refund])
        return total

    @property
    def space_cost_by_day(self):
        created = self.created
        end = self.pre_high_date if self.pre_high_date is not None else timezone.now()

        if created:
            days = (end - created).days
            hours = (end - created).seconds / 60 / 60
            if days == 0:
                return (days + 1) * self.space_cost
            else:
                if hours < 6:
                    return days * self.space_cost
                else:
                    return (days + 1) * self.space_cost
        return 0

    # endregion

    @property
    def can_pay_honorariums(self):
        """
        Return True if the honorariums can be paid otherwise return false.
        NOTE: For Hospitalizations created before 30/06/2021 will always return True.
        :return: bool
        """

        if not len(self.charge_set.filter(charge_type=1, paid__isnull=True)):
            return False

        before = make_aware(timezone.datetime(2021, 6, 30))
        if self.created <= before:
            return True
        can_pay = self.hospital_charges < self.payments_total
        return can_pay

    @property
    def days_since_entry(self):
        """
        Return the days since the Patient is hospitalized
        :return: int
        """
        days_since_entry = (timezone.now() - self.created).days
        return days_since_entry

    @property
    def days_since_pre_high(self):
        """
        Return the days since the Patient was changed to "Pre-alta" state.
        :return: int or str if the user is not in the 'PRE-HIGH' state
        """
        if self.pre_high_date:
            days_since_pre_high = (timezone.now() - self.pre_high_date).days
            return days_since_pre_high
        return '-'


class ChargeOld(HospitalBaseModel):
    total = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    refund = models.BooleanField(default=False)

    # region PRODUCT FIELDS
    cost = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    application_cost = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    quantity = models.PositiveIntegerField(default=0)
    # endregion

    # Relationship
    hospitalization = models.ForeignKey('patients.Hospitalization', on_delete=models.CASCADE)
    product = models.ForeignKey('warehouse.Product', on_delete=models.CASCADE, blank=True, null=True)
    warehouse = models.ForeignKey('warehouse.Warehouse', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        pass

    def __str__(self):
        return f'{self.name}: {self.total}'


class Charge(HospitalBaseModel):
    subtotal = models.DecimalField(max_digits=10, decimal_places=2)
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    is_refund = models.BooleanField(default=False)
    paid = models.DateTimeField(blank=True, null=True)

    # Field if it's product
    cost = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    application_cost = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    quantity = models.PositiveIntegerField(default=0)

    # Relationships
    hospitalization = models.ForeignKey('patients.Hospitalization', on_delete=models.CASCADE)
    charge_type = models.ForeignKey('patients.ChargeType', on_delete=models.CASCADE, related_name='charges')

    # Relationship optionals
    product = models.ForeignKey('warehouse.Product', on_delete=models.CASCADE, blank=True, null=True)
    warehouse = models.ForeignKey('warehouse.Warehouse', on_delete=models.CASCADE, blank=True, null=True)
    physician = models.ForeignKey('profiles.CustomUser', on_delete=models.CASCADE, blank=True, null=True)
    refund = models.OneToOneField('self', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        ordering = ['created']

    def __str__(self):
        return f'{self.id}- {self.name}'

    def __init__(self, *args, **kwargs):
        super(Charge, self).__init__(*args, **kwargs)
        self.old_quantity = self.quantity

    def total(self):
        if self.is_refund:
            return self.subtotal * -1
        return self.subtotal

    def clean(self) -> None:
        # Validate for honorarium
        if hasattr(self, 'charge_type'):
            if self.charge_type.id == 1:
                messages = {}
                if not self.physician:
                    messages['physician'] = ValidationError(
                        'If the charge it\'s honorarium charge, a Physician is required')
                if self.product:
                    messages['product'] = ValidationError(
                        'If the charge it\'s honorarium charge, you cannot specify a Product')
                if self.warehouse:
                    messages['warehouse'] = ValidationError(
                        'If the charge it\'s honorarium charge, you cannot specify a Warehouse')
                if len(messages) > 0:
                    raise ValidationError(messages)
            elif self.charge_type.id == 2:
                messages = {}
                if not self.product:
                    messages['product'] = ValidationError('If the charge it\'s supplies charge, a Product is required')
                    # raise ValidationError(messages)
                if not self.warehouse:
                    messages['warehouse'] = ValidationError(
                        'If the charge it\'s supplies charge, a Warehouse is required')
                if not self.quantity:
                    messages['quantity'] = ValidationError(
                        'If the charge it\'s supplies charge, a quantity greater than 0 is required')
                if self.physician:
                    messages['physician'] = ValidationError(
                        'If the charge it\'s supplies charge, you cannot specify a physician')
                if not self.cost > 0:
                    messages['cost'] = ValidationError(
                        'If the charge it\'s supplies charge, a charge greater than 0 is required')

                diff = self.old_quantity - self.quantity
                if diff < 0:
                    stock = self.product.stock_set.filter(warehouse=self.warehouse).first()
                    if stock and stock.quantity < (diff * - 1):

                        messages['quantity'] = ValidationError(
                            f'Not available stock for product: "{self.product}" in warehouse: "{self.warehouse}", '
                            f'only {stock.quantity} left.')
                if len(messages) > 0:
                    raise ValidationError(messages)

            elif self.charge_type.id == 3:
                # TODO: Add Charge type 3 validations
                pass


class ChargeType(HospitalBaseModel):
    name = models.CharField(max_length=100)

    class Meta:
        pass

    def __str__(self):
        return f'{self.name}'


class Payment(HospitalBaseModel):
    amount = models.DecimalField(max_digits=10, decimal_places=2)

    # Relationships
    hospitalization = models.ForeignKey('patients.Hospitalization', on_delete=models.CASCADE)

    class Meta:
        pass

    def __str__(self):
        return f'{self.amount}'


class Discount(HospitalBaseModel):
    name = models.CharField(max_length=200)
    percentage = models.PositiveSmallIntegerField(
        blank=True,
        null=True,
        validators=[MinValueValidator(limit_value=0, message=PERCENTAGE_ERROR_MESSAGE),
                    MaxValueValidator(limit_value=100, message=PERCENTAGE_ERROR_MESSAGE)],

    )
    amount = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)

    # Relationships
    hospitalization = models.ForeignKey('patients.Hospitalization', on_delete=models.CASCADE)

    class Meta:
        pass

    def __str__(self):
        if self.percentage:
            return f'{self.name}: {self.percentage}%'
        return f'{self.name}: {self.amount}'

    def clean(self) -> None:
        if self.percentage and self.amount:
            error = 'A discount can only have "percentage" or "amount" not both at the same time.'
            raise ValidationError(error)

        if not self.percentage and not self.amount:
            error = 'You must specify a "percentage" or an "amount".'
            raise ValidationError(error)
        super().clean()


class Document(HospitalBaseModel):
    content = models.TextField()
    hospitalization = models.ForeignKey('patients.Hospitalization', on_delete=models.CASCADE)

    class Meta:
        pass
