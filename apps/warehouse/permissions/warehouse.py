# -*- coding: utf-8 -*-
from django.contrib.auth.models import Group
from rest_framework import permissions

from apps.common.groups_variables import MEDIC_GROUP_ID, NURSING_GROUP_ID


class WarehouseViewSetPermission(permissions.BasePermission):
    message = 'User doesn\'t have permission'
    allowed_groups = Group.objects.filter(id__in=[MEDIC_GROUP_ID, NURSING_GROUP_ID]).all()

    def has_permission(self, request, view):
        user = request.user
        groups = user.groups.all()
        for group in groups:
            if group in self.allowed_groups:
                return True
        return user.is_superuser
