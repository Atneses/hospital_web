# -*- coding: utf-8 -*-
from typing import Type

from django.db.models import QuerySet
from django.db.models.query_utils import Q
from rest_framework import viewsets, status
from rest_framework.generics import ListAPIView
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.serializers import BaseSerializer
from rest_framework.views import APIView

from apps.warehouse.models import Product, Presentation, PRODUCT, MEDICINE, Warehouse, Stock
from apps.warehouse.permissions.permissions import WH_USR_CAN_READ_ALL_WH
from apps.warehouse.serializers import GenericProductSerializer, PresentationSerializer, ProductSerializer, \
    MedicineSerializer, WarehouseSerializer, StockSerializer, GenericProductShallowSerializer, ProductShallowSerializer, \
    MedicineShallowSerializer


# Create your views here.
class GenericProductViewSet(viewsets.ModelViewSet):
    """
    ViewSet of products to return all type of products, medicines and products
    """
    permission_classes = [IsAuthenticated]
    queryset = Product.objects.all()
    serializer_class = GenericProductShallowSerializer
    pagination_class = LimitOffsetPagination

    def get_serializer_class(self) -> Type[BaseSerializer]:
        if self.action != 'list':
            return GenericProductSerializer
        return super().get_serializer_class()


class ProductViewSet(viewsets.ModelViewSet):
    """
    ViewSet of products to return only products
    """
    permission_classes = [IsAuthenticated]
    queryset = Product.objects.filter(product_type=PRODUCT).all()
    serializer_class = ProductShallowSerializer
    pagination_class = LimitOffsetPagination

    def get_serializer_class(self) -> Type[BaseSerializer]:
        if self.action != 'list':
            return ProductSerializer
        return super().get_serializer_class()


class InsumoViewSet(viewsets.ModelViewSet):
    """
    ViewSet of products to return only medicine
    """
    permission_classes = [IsAuthenticated]
    queryset = Product.objects.filter(product_type=PRODUCT).all()
    serializer_class = ProductShallowSerializer
    pagination_class = LimitOffsetPagination

    def create(self, request, **kwargs):
        response = {'message': 'Create function is not offered in this path.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)

    def update(self, request, pk=None, **kwargs):
        response = {'message': 'Update function is not offered in this path.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)

    def partial_update(self, request, pk=None, **kwargs):
        response = {'message': 'Update function is not offered in this path.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, pk=None, **kwargs):
        response = {'message': 'Delete function is not offered in this path.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)


class MedicineViewSet(viewsets.ModelViewSet):
    """
    ViewSet of products to return only medicine
    """
    permission_classes = [IsAuthenticated]
    queryset = Product.objects.filter(product_type=MEDICINE).all()
    serializer_class = MedicineShallowSerializer
    pagination_class = LimitOffsetPagination

    def get_serializer_class(self) -> Type[BaseSerializer]:
        if self.action != 'list':
            return MedicineSerializer
        return super().get_serializer_class()


class PresentationViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Presentation.objects.all()
    serializer_class = PresentationSerializer


class WarehouseViewSet(viewsets.ModelViewSet):
    """
        list: Return a list of the warehouses.<br/>
        If the user is the user is *superadmin* it return all the *warehouses*.<br/>
        If **IS NOT** a *superadmin* it return the *warehouses* that the user is in.

        create: Create a new warehouse the only required field is **name**.

        read: Get a warehouse bases of the **id** sent it as a parameter.<br/>
        A super admin can see any warehouse.<br/>
        If **IS NOT** a *superadmin* it only can see the warehouse where he is in

        update: Update a warehouse using the **id** sent it as a parameter.

        partial_update: Update a warehouse using the **id** sent it as a parameter.

        delete: Delete a warehouse using the **id** sent it as a parameter.
    """
    permission_classes = [IsAuthenticated]
    queryset = Warehouse.objects.all()
    serializer_class = WarehouseSerializer

    def get_queryset(self) -> QuerySet:
        """
        Custom filter, is the user is super admin it return all the warehouses
        if is not a super admin it return the warehouses that the user is in
        :return:
        """
        # TODO: Filter access to users to only have access to their warehouse, right now the superuser and the medic
        #  user had access to all warehouses
        groups = self.request.user.groups.filter(id__in=[1, 2])

        user = self.request.user

        if user.has_perm(f'warehouse.{WH_USR_CAN_READ_ALL_WH}'):
            data = super().get_queryset()
            return data
        else:
            query_set = super().get_queryset()
            query_set = query_set.filter(customuser=self.request.user).all()
            return query_set


class WarehouseProductsAdminView(APIView):
    permission_classes = [IsAuthenticated, IsAdminUser]

    def get(self, *args, **kwargs):
        wh_id = kwargs['pk']
        _type = kwargs['type']

        stock = Stock.objects.filter(self.get_filter(wh_id, _type)).all()
        serializer = StockSerializer(stock, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_filter(self, wh_id, _type):
        q = Q(warehouse__id=wh_id) & Q(product__product_type=_type)
        return q


class WarehouseProductsUserView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, *args, **kwargs):
        wh = self.request.user.warehouse
        if not wh:
            stock = Stock.objects.none()
            return Response(StockSerializer(stock, many=True).data, status=status.HTTP_200_OK)

        _type = kwargs['type']
        q = Q(warehouse=wh) & Q(product__product_type=_type)
        stock = Stock .objects .filter(q) .all()
        serializer = StockSerializer(stock, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class StockViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Stock.objects.all()
    serializer_class = StockSerializer

    def get_queryset(self) -> QuerySet:
        """
        Custom filter, is the user is super admin it return all the stocks
        if is not a super admin it return the stocks that the user is in
        :return:
        """
        current_user = self.request.user
        if current_user.is_superuser:
            return super().get_queryset()
        else:
            wh = Warehouse.objects.filter(customuser=current_user).first()
            stock = wh.stock_set.all()
            return stock


class BarcodeSearchView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, *args, **kwargs):
        barcode = kwargs['barcode']
        product = Product.objects.filter(barcode=barcode).first()
        if product:
            if product.product_type == PRODUCT:
                serializer = ProductSerializer(product)
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                serializer = MedicineSerializer(product)
                return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)


# region Searches
class GenericProductSearch(ListAPIView):
    """
    Generic product search.
    If the product_type property is define with the possible values PRODUCT or
    MEDICINE, it will add a filter to filter by product or medicine, otherwise
    it will return al type of products
    """
    permission_classes = [IsAuthenticated]
    queryset = Product.objects.all()
    serializer_class = GenericProductShallowSerializer
    pagination_class = LimitOffsetPagination
    product_type = None

    def get_queryset(self) -> QuerySet:
        search = self.kwargs['search']
        q = Q(name__icontains=search) | Q(brand__icontains=search) | Q(barcode__icontains=search)
        if self.product_type:
            q = q & Q(product_type=self.product_type)
        result = Product.objects.filter(q).all()

        return result


class MedicineSearch(GenericProductSearch):
    serializer_class = MedicineShallowSerializer
    product_type = MEDICINE


class ProductSearch(GenericProductSearch):
    serializer_class = ProductShallowSerializer
    product_type = PRODUCT

    def get_queryset(self) -> QuerySet:
        return super().get_queryset()

# endregion
