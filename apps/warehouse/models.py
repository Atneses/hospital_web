from django.db import models
from safedelete import SOFT_DELETE
from safedelete.models import SafeDeleteModel

from apps.common.common_models import HospitalBaseModel
from apps.warehouse.permissions.permissions import WH_USR_CAN_READ_ALL_WH, WH_USR_CAN_READ_ALL_WH_NAME

PRODUCT = 'PRODUCT'
MEDICINE = 'MEDICINE'
PRODUCT_TYPES = ((PRODUCT, 'Producto'), (MEDICINE, 'Medicamento'))


# Create your models here.
class Stock(HospitalBaseModel):
    quantity = models.IntegerField()

    # Relationships
    warehouse = models.ForeignKey('warehouse.Warehouse', on_delete=models.CASCADE)
    product = models.ForeignKey('warehouse.Product', on_delete=models.CASCADE)

    class Meta:
        # Constrain that make that can not be duplicate products in the same warehouse
        constraints = [
            models.UniqueConstraint(
                fields=['warehouse', 'product'],
                name='unique_product_in_warehouse'
            )
        ]

    def __str__(self):
        return f'{self.product}: {self.quantity}'

    @staticmethod
    def add_to_stock(product, warehouse, quantity):
        """
        Return the given quantity for the product to the given warehouse
        :param product:
        :param warehouse:
        :param quantity:
        :return:
        """
        stock = product.stock_set.filter(warehouse=warehouse).first()
        if stock:
            stock.quantity = stock.quantity + quantity
            stock.save()

    @staticmethod
    def subtract_from_stock(product, warehouse, quantity):
        """
        Return the given quantity for the product to the given warehouse
        :param product:
        :param warehouse:
        :param quantity:
        :return:
        """
        stock = product.stock_set.filter(warehouse=warehouse).first()
        if stock:
            stock.quantity = stock.quantity - quantity
            stock.save()

    @staticmethod
    def validate_stock(product, warehouse, quantity):
        pass


class Product(HospitalBaseModel):
    _safedelete_policy = SOFT_DELETE

    # region Common fields
    name = models.CharField(max_length=100)
    brand = models.CharField(max_length=100)
    barcode = models.CharField(max_length=100, unique=True)
    product_type = models.CharField(
        max_length=50,
        choices=PRODUCT_TYPES,
        help_text=f'Select field, available choices are: "{PRODUCT}" and "{MEDICINE}"'
    )
    presentation = models.CharField(max_length=100, blank=True, null=True)
    cost = models.DecimalField(max_digits=10, decimal_places=2)
    application_cost = models.DecimalField(max_digits=10, decimal_places=2)
    # endregion

    # region Medicine fields
    active_substance = models.CharField(max_length=100, blank=True, null=True)
    dosage = models.CharField(max_length=100, blank=True, null=True)
    laboratory = models.CharField(max_length=100, blank=True, null=True)
    administration_way = models.CharField(max_length=100, blank=True, null=True)
    # endregion

    # Relationships
    presentation_medicine = models.ForeignKey(
        'warehouse.Presentation',
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )

    class Meta:
        pass

    def __str__(self):
        return str(f'{self.name}')

    def save(self, keep_deleted=False, **kwargs):
        # TODO: Add validation, raise exception when type is MEDICINE and have presentation
        # TODO: Add validation, raise exception when is PRODUCT and have presentation_medicine
        super().save(keep_deleted, **kwargs)

    @property
    def has_any_stock(self):
        stocks = self.stock_set.filter(quantity__gte=1).all()
        if stocks:
            return True
        return False

    def has_stock_in_warehouse(self, warehouse):
        stock = self.stock_set.filter(warehouse=warehouse).first()
        if stock:
            if stock.quantity >= 1:
                return True
            return False
        return False


class Presentation(HospitalBaseModel):
    presentation_type = models.CharField(max_length=100)

    class Meta:
        pass

    def __str__(self):
        return str(f'{self.presentation_type}')


class Warehouse(HospitalBaseModel):
    name = models.CharField(max_length=100, help_text='Char field with max length of: 100')

    class Meta:
        pass
        permissions = [
            (
                WH_USR_CAN_READ_ALL_WH,
                WH_USR_CAN_READ_ALL_WH_NAME
            )
        ]

    def __str__(self):
        return f'{self.name}'
