# -*- coding: utf-8 -*-
from typing import Any

from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import ModelSerializer

from apps.common.common_serializers import HospitalBaseSerializer
from apps.profiles.serializers import CustomUserSerializer
from apps.warehouse.models import Product, Presentation, Warehouse, Stock


class GenericProductSerializer(HospitalBaseSerializer):
    presentation_medicine = PrimaryKeyRelatedField(queryset=Presentation.objects.all())

    class Meta(HospitalBaseSerializer.Meta):
        model = Product

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        if instance.presentation_medicine:
            data['presentation_medicine'] = PresentationSerializer(instance.presentation_medicine).data
        return data


class GenericProductShallowSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class ProductSerializer(HospitalBaseSerializer):
    class Meta(HospitalBaseSerializer.Meta):
        model = Product
        fields = None
        exclude = [
            'active_substance',
            'dosage',
            'laboratory',
            'administration_way',
            'presentation_medicine'
        ]


class ProductShallowSerializer(ModelSerializer):
    class Meta:
        model = Product
        exclude = [
            'active_substance',
            'dosage',
            'laboratory',
            'administration_way',
            'presentation_medicine'
        ]


class MedicineSerializer(GenericProductSerializer):
    class Meta(HospitalBaseSerializer.Meta):
        model = Product
        fields = None
        exclude = ['presentation']


class MedicineShallowSerializer(ModelSerializer):
    class Meta:
        model = Product
        exclude = ['presentation']

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['presentation_medicine'] = PresentationShallowSerializer(instance.presentation_medicine).data
        return data


class PresentationSerializer(HospitalBaseSerializer):
    class Meta(HospitalBaseSerializer.Meta):
        model = Presentation


class PresentationShallowSerializer(ModelSerializer):
    class Meta:
        model = Presentation
        fields = '__all__'


class StockSerializer(HospitalBaseSerializer):
    product = PrimaryKeyRelatedField(queryset=Product.objects.all())
    warehouse = PrimaryKeyRelatedField(queryset=Warehouse.objects.all())

    class Meta(HospitalBaseSerializer.Meta):
        model = Stock

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['product'] = ProductSerializer(instance.product).data
        data['warehouse'] = WarehouseSerializerForStock(instance.warehouse).data
        return data


class WarehouseSerializerForStock(HospitalBaseSerializer):
    """
    Warehouse serializer to use in the Stock Serializer to avoid recursion error
    """

    class Meta(HospitalBaseSerializer.Meta):
        model = Warehouse


class WarehouseSerializer(HospitalBaseSerializer):
    """
    Warehouse full serializer to use in the warehouse endpoints
    """
    stock_set = StockSerializer(many=True, read_only=True)
    customuser_set = CustomUserSerializer(many=True, read_only=True)

    class Meta(HospitalBaseSerializer.Meta):
        model = Warehouse
