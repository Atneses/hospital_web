from django.contrib import admin
from safedelete.admin import SafeDeleteAdmin

from apps.common.common_models import BASE_FIELDS
from apps.warehouse.models import Product, Presentation, Warehouse, Stock


# Register your models here.
@admin.register(Product)
class ProductAdmin(SafeDeleteAdmin):
    list_display = [
        'id',
        'barcode',
        'name',
        'brand',
        'presentation',
        'cost',
        'application_cost',
        'has_any_stock',
        'deleted'
    ]
    search_fields = [
        'id',
        'barcode',
        'name',
        'brand',
        'presentation',
        'cost',
        'application_cost'
    ]
    readonly_fields = BASE_FIELDS


@admin.register(Presentation)
class PresentationAdmin(SafeDeleteAdmin):
    list_display = ['id', 'presentation_type', 'deleted']
    search_fields = ['id', 'presentation_type']
    readonly_fields = BASE_FIELDS


@admin.register(Warehouse)
class WarehouseAdmin(SafeDeleteAdmin):
    list_display = ['id', 'name', 'deleted']
    search_fields = ['id', 'name']
    readonly_fields = BASE_FIELDS


@admin.register(Stock)
class StockAdmin(SafeDeleteAdmin):
    list_display = ['id', 'product', 'quantity', 'warehouse', 'deleted']
    search_fields = [
        'id',
        'product__barcode',
        'product__name',
        'product__brand',
        'quantity',
        'warehouse__name'
    ]
    readonly_fields = BASE_FIELDS
