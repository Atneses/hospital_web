# -*- coding: utf-8 -*-
from django.urls import path, include
from rest_framework import routers

from apps.warehouse import views

router = routers.DefaultRouter()
router.register('generic-products', views.GenericProductViewSet)
router.register('products', views.ProductViewSet)
router.register('medicines', views.MedicineViewSet)
router.register('insumos', views.InsumoViewSet)
router.register('presentations', views.PresentationViewSet)
router.register('warehouses', views.WarehouseViewSet)
router.register('stocks', views.StockViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('warehouses/<int:pk>/<str:type>/', views.WarehouseProductsAdminView.as_view(), name='warehouse_filter_products'),
    path('user-warehouse/<str:type>/', views.WarehouseProductsUserView.as_view(), name='warehouse_user_filter_products'),
    path('barcode-search/<str:barcode>', views.BarcodeSearchView.as_view(), name='barcode-search'),
    path('generic-products-search/<str:search>', views.GenericProductSearch.as_view(), name='generic-products-search'),
    path('medicine-search/<str:search>', views.MedicineSearch.as_view(), name='medicine-search'),
    path('product-search/<str:search>', views.ProductSearch.as_view(), name='product-search')
]
