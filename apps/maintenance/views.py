# -*- coding: utf-8 -*-
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.maintenance.models import Maintenance


# Create your views here.
class MaintenanceView(APIView):
    def get(self, request):
        is_in_maintenance = Maintenance.objects.order_by('-created').first()
        if is_in_maintenance:
            return Response(is_in_maintenance.maintenance, status=status.HTTP_200_OK)
        return Response(False, status=status.HTTP_200_OK)
