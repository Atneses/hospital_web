from django.contrib import admin
from safedelete.admin import SafeDeleteAdmin

from apps.common.common_models import BASE_FIELDS
from apps.maintenance.models import Maintenance


# Register your models here.
@admin.register(Maintenance)
class MaintenanceAdmin(SafeDeleteAdmin):
    list_display = ['maintenance', 'created', 'modified']
    readonly_fields = [] + BASE_FIELDS
