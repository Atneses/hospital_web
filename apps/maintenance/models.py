# -*- coding: utf-8 -*-
from apps.common.common_models import HospitalBaseModel

from django.db import models


# Create your models here.
class Maintenance(HospitalBaseModel):
    maintenance = models.BooleanField(default=True)

    def __str__(self):
        return f'Is in Maintenance: {self.maintenance}'
