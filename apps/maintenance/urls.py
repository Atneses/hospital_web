# -*- coding: utf-8 -*-
from django.urls import path

from apps.maintenance.views import MaintenanceView

urlpatterns = [
    path('is-in-maintenance/', MaintenanceView.as_view(), name='is-in-maintenance')
]
