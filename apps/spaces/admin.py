# -*- coding: utf-8 -*-
from django.contrib import admin
from safedelete.admin import SafeDeleteAdmin

from apps.common.common_models import BASE_FIELDS
from apps.spaces.models import Space, Area, Type


# Register your models here.
@admin.register(Space)
class SpaceAdmin(SafeDeleteAdmin):
    list_display = ['number', 'area', 'space_type', 'cost', 'deleted']
    search_fields = ['number', 'area__area_name', 'space_type__type_name']
    readonly_fields = ['is_assigned', 'space_name'] + BASE_FIELDS


@admin.register(Area)
class AreaAdmin(SafeDeleteAdmin):
    list_display = ['area_name', 'deleted']
    search_fields = ['area_name']
    readonly_fields = BASE_FIELDS


@admin.register(Type)
class TypeAdmin(SafeDeleteAdmin):
    list_display = ['type_name', 'deleted']
    search_fields = ['type_name']
    readonly_fields = BASE_FIELDS

