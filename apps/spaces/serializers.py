# -*- coding: utf-8 -*-
from typing import Any

from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from apps.spaces.models import Space, Area, Type
from apps.common.common_serializers import HospitalBaseSerializer


class AreaSerializer(HospitalBaseSerializer):
    class Meta(HospitalBaseSerializer.Meta):
        model = Area


class TypeSerializer(HospitalBaseSerializer):
    class Meta(HospitalBaseSerializer.Meta):
        model = Type


class SpaceSerializer(HospitalBaseSerializer):
    area = PrimaryKeyRelatedField(queryset=Area.objects.all())
    space_type = PrimaryKeyRelatedField(queryset=Type.objects.all())
    is_assigned = serializers.BooleanField(read_only=True)
    space_name = serializers.CharField(read_only=True)

    class Meta(HospitalBaseSerializer.Meta):
        model = Space

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['area'] = AreaSerializer(instance.area).data
        data['space_type'] = TypeSerializer(instance.space_type).data
        return data
