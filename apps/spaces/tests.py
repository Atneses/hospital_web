from crum import impersonate
from faker import Faker
from rest_framework import status
from rest_framework.reverse import reverse

from apps.common.common_tests import CustomBaseTest
from apps.spaces.models import Area, Type

faker = Faker()


# Create your tests here.
def create_areas(user):
    with impersonate(user):
        for x in range(0, 10):
            data = {'area_name': faker.name()}
            Area.objects.create(**data)


def create_types(user):
    with impersonate(user):
        for x in range(0, 10):
            data = {'type_name': faker.name()}
            Type.objects.create(**data)


class AreaTest(CustomBaseTest):

    def setUp(self) -> None:
        super().setUp()
        create_areas(self.admin)

    def test_create_area(self):
        url = reverse('spaces:area-list')
        data = {'area_name': faker.name()}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_area(self):
        url = reverse('spaces:area-detail', args=[1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_areas(self):
        url = reverse('spaces:area-list')
        response = self.client.get(url, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_area(self):
        url = reverse('spaces:area-detail', args=[1])
        new_name = 'new area name'
        data = {'area_name': new_name}
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['area_name'], new_name)

    def test_delete_area(self):
        url = reverse('spaces:area-detail', args=[1])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class TypeTest(CustomBaseTest):

    def setUp(self) -> None:
        super().setUp()
        # Create initial types
        # create_types(self.admin)

    def test_create_type(self):
        url = reverse('spaces:type-list')
        data = {'type_name': faker.name()}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_type(self):
        url = reverse('spaces:type-detail', args=[1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('id', response.data)

    def test_list_types(self):
        url = reverse('spaces:type-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_type(self):
        url = reverse('spaces:type-detail', args=[1])
        new_name = 'new type name'
        data = {'type_name': new_name}
        response = self.client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['type_name'], new_name)

    def test_delete_type(self):
        url = reverse('spaces:type-detail', args=[1])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class SpaceTest(CustomBaseTest):

    def setUp(self) -> None:
        super().setUp()

        # Create initial areas
        create_areas(self.admin)

    def test_create_space(self):
        pass
