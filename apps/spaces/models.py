from django.db import models
from safedelete.models import SafeDeleteModel

from apps.common.common_models import HospitalBaseModel


# Create your models here.
class Space(HospitalBaseModel):
    number = models.CharField(max_length=50)
    cost = models.DecimalField(max_digits=10, decimal_places=2)

    # Relationships
    area = models.ForeignKey('spaces.Area', on_delete=models.CASCADE)
    space_type = models.ForeignKey('spaces.Type', on_delete=models.CASCADE)

    class Meta:
        pass

    def __str__(self):
        return str(f'{self.number}')

    def is_assigned(self):
        if hasattr(self, 'hospitalization'):
            return True
        return False

    @property
    def space_name(self):
        return f'{self.area.area_name} {self.space_type.type_name} {self.number}'


class Area(HospitalBaseModel):
    area_name = models.CharField(max_length=100)

    class Meta:
        pass

    def __str__(self):
        return self.area_name


class Type(HospitalBaseModel):
    type_name = models.CharField(max_length=100)

    class Meta:
        pass

    def __str__(self):
        return self.type_name
