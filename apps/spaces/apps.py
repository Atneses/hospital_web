from django.apps import AppConfig


class SpacesConfig(AppConfig):
    name = 'apps.spaces'
