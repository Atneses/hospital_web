# -*- coding: utf-8 -*-
from django.urls import path, include
from rest_framework import routers

from apps.spaces import views

router = routers.DefaultRouter()
router.register('spaces', views.SpaceViewSet)
router.register('areas', views.AreaViewSet)
router.register('types', views.TypeViewSet)

urlpatterns = [
    path('', include(router.urls))
]
