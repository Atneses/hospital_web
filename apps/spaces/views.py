# -*- coding: utf-8 -*-
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from apps.spaces.models import Space, Area, Type
from apps.spaces.serializers import SpaceSerializer, AreaSerializer, TypeSerializer


# Create your views here.
class SpaceViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Space.objects.all()
    serializer_class = SpaceSerializer


class AreaViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Area.objects.all()
    serializer_class = AreaSerializer


class TypeViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Type.objects.all()
    serializer_class = TypeSerializer
