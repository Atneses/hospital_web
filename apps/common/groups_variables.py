# -*- coding: utf-8 -*-

# region Groups names
# Médico
MEDIC_GROUP_ID = 1
MEDIC_GROUP_NAME = 'Médico'

# Enfermería
NURSING_GROUP_ID = 2
NURSING_GROUP_NAME = 'Enfermería'

# Recepción
RECEPTION_GROUP_ID = 3
RECEPTION_GROUP_NAME = 'Recepción'

# Almacén
WAREHOUSE_GROUP_ID = 4
WAREHOUSE_GROUP_NAME = 'Almacén'

# Caja
CHECKOUT_GROUP_ID = 5
CHECKOUT_GROUP_NAME = 'Caja'

# Administración
ADMINISTRATION_GROUP_ID = 6
ADMINISTRATION_GROUP_NAME = 'Administración'

# Dirección
MANAGEMENT_GROUP_ID = 7
MANAGEMENT_GROUP_NAME = 'Dirección'
# endregion
