# -*- coding: utf-8 -*-
from rest_framework.test import APITestCase

from apps.profiles.models import CustomUser


class CustomBaseTest(APITestCase):
    """
    Custom base test class that include the creation and the initial authentication
    of an superuser
    """
    def setUp(self) -> None:
        self.username = 'admin'
        self.email = 'admin@email.com'
        self.password = 'admin1'
        user_data = {
            'username': self.username,
            'email': self.email,
            'password': self.password
        }
        self.admin = CustomUser.objects.create_superuser(**user_data)
        self.client.login(username=self.username, password=self.password)
