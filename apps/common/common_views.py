# -*- coding: utf-8 -*-
from typing import Any

from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet


class FilterMixin:
    def filter_queryset(self, queryset):
        """
        Allow to a view set to receive the params "field" and "order", to filter and modify the queryset.
        :param queryset:
        :return: queryset:
        """
        order = self.request.GET.get('order')
        if order:
            field = 'created'
            if 'field' in self.request.GET:
                field = self.request.GET.get('field')

            queryset = super(FilterMixin, self).filter_queryset(queryset)
            ordering = f'{"-" if order.lower() == "desc" else ""}{field}'
            queryset = queryset.order_by(ordering)
            return queryset
        return super(FilterMixin, self).filter_queryset(queryset)


class HospitalBaseViewSet(FilterMixin, ModelViewSet):
    """
    Base ViewSet class for the hospital.
    Allow to create object in batch.
    """
    pagination_class = LimitOffsetPagination

    def create(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        current_serializer = self.get_serializer_class()
        serializer = current_serializer(data=request.data, many=isinstance(request.data, list))
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_serializer_class(self):
        if hasattr(self, 'small_serializer_class'):
            if self.action == 'list':
                return self.small_serializer_class
        return super().get_serializer_class()


class HospitalBaseListAPIView(FilterMixin, ListAPIView):
    pass
