# -*- coding: utf-8 -*-
from typing import Any

from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import ModelSerializer

from apps.profiles.models import CustomUser


class CustomUserShortSerializer(ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['id', 'username', 'first_name', 'last_name']


class HospitalBaseSerializer(ModelSerializer):
    created_by = PrimaryKeyRelatedField(read_only=True)
    modified_by = PrimaryKeyRelatedField(read_only=True)

    class Meta:
        fields = '__all__'
        depth = 1

    def to_representation(self, instance: Any) -> Any:
        data = super().to_representation(instance)
        data['created_by'] = CustomUserShortSerializer(instance.created_by).data
        data['modified_by'] = CustomUserShortSerializer(instance.created_by).data
        return data
