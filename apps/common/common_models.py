# -*- coding: utf-8 -*-
from crum import get_current_user
from django.conf import settings
from django.db import models

from safedelete.models import SafeDeleteModel

BASE_FIELDS = ['deleted', 'created_by', 'modified_by', 'created', 'modified']


class HospitalBaseModel(SafeDeleteModel):
    """
    Base model that include created and modified fields
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    # Dynamic related name, link to the documentation
    # https://docs.djangoproject.com/en/2.2/topics/db/models/#be-careful-with-related-name-and-related-query-name
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='%(app_label)s_%(class)s_created_by'
    )
    modified_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='%(app_label)s_%(class)s_modified_by'
    )

    class Meta:
        abstract = True

    def save(self, keep_deleted=False, **kwargs):
        """
        Override the save method to set the created_by field to the current user
        and the modified_by to the current user.
        :param keep_deleted:
        :param kwargs:
        :return:
        """
        if self._state.adding:
            self.created_by = get_current_user()
        self.modified_by = get_current_user()
        super().save(keep_deleted, **kwargs)
