"""hospital_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from rest_framework.documentation import include_docs_urls
from rest_framework.schemas import get_schema_view
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

schema_view = get_schema_view(
    title='test',
    description='test2'
)
schema_title = 'Hospital API'

urlpatterns = [
    # Schema
    path('api/', get_schema_view(
        title=schema_title,
        description=schema_title,
        version='1.0.0'
    )),
    path('api/docs', include_docs_urls(title=schema_title)),

    path('admin/', admin.site.urls),
    # Rest framework auth
    path('api/auth/', include('rest_framework.urls', namespace='rest_framework')),

    # JWT Authentication
    path('api/token/', TokenObtainPairView.as_view(), name='token'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    # Apps
    path('profiles/', include(('apps.profiles.urls', 'apps.profiles'), namespace='profiles')),
    path('spaces/', include(('apps.spaces.urls', 'apps.spaces'), namespace='spaces')),
    path('warehouse/', include(('apps.warehouse.urls', 'apps.warehouse'), namespace='warehouse')),
    path('purchase/', include(('apps.purchase.urls', 'apps.purchase'), namespace='purchase')),
    path('patients/', include(('apps.patients.urls', 'apps.patients'), namespace='patient')),
    path('maintenance/', include(('apps.maintenance.urls', 'apps.maintenance'), namespace='maintenance')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]
