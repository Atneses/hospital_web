# Hospital Web API


How to run for development

## Requirements

### Base requirements
* django==2.2
* djangorestframework==3.11.0
* PyYAML==5.2
* coreapi==2.3.3
* Pygments==2.5.2
* django-stubs==1.4.0
* mypy==0.761
* typing-extensions==3.7.4.1
* djangorestframework-simplejwt==4.4.0
* django-safedelete==0.5.4
* django-cors-headers==3.2.1
* django-crum==0.7.5


## Install dependencies
To install the dependencies for the base and develop environment you must run the next command:

`pip install -r requierements\base.txt`
`pip install -r requierements\develop.txt`

Alternatively, you can set up a Conda environment using the following commands:

``
conda create --name holi python=3.8;
conda install --name holi -c anaconda django pyyaml pygments psycopg2;
conda install --name holi -c conda-forge djangorestframework python-coreapi mypy typing-extensions djangorestframework_simplejwt django-cors-headers xhtml2pdf faker;
conda install --name holi -c bioconda mysqlclient;
conda activate holi;
conda install pip;
pip install django-stub;
pip install django-safedelete;
pip install django-crum;
``

## Create local settings file
To override the base configuration you need to create a local settings file called: `local_settings.py`
Put the next code to override the default database and create a local one.

Using __sqlite__:

```
# -*- coding: utf-8 -*-
import os

from hospital_web import settings

settings.DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(settings.BASE_DIR, 'db.sqlite3'),
    }
}

```

Using __mysql__:

```
# -*- coding: utf-8 -*-
import os

from hospital_web import settings

settings.DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'holi',
        'USER': 'root',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}
```

## Create local database
Create your local database using the command:

`python manage.py migrate`

This command will create your local database and apply all the needed migrations for the project

## Run fake data generator **(ONLY FOR DEVELOP ENVIRONMENT)**
### Fake data generator
To run the fake data generator run the next command

`python manage.py fake_data`

This will create fake data for the next models:
* users
* areas
* types
* spaces
* warehouse
* presentation_medicine
* products
* medicine
* stocks

### Run fake patients generator
To run the fake data generator for patients run the next command

`python manage.py fake_patients`
This will create fake data for the next models:
* patients
* relatives
* hospitalizations

## Run the server
To run the server in develop environment use the next command:

`python manage.py runserver`

This will run the development server in the `127.0.0.1:8000` URL.